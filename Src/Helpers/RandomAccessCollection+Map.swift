//
//  RandomAccessCollection+Map.swift
//  AppCore
//
//  Created by loki on 20.12.2020.
//  Copyright © 2020 Loki. All rights reserved.
//

import Foundation

public struct MapCollection<Data, OutElement> where Data : RandomAccessCollection {
    public let data : Data
    public let block: (Data.Element, Int) -> (Self.Element)
    public let limit: Int
    
    public init(data: Data, limit: Int = Int.max, block: @escaping (Data.Element, Int) -> (Self.Element)) {
        self.data = data
        self.block = block
        self.limit = limit
    }
}

extension MapCollection : RandomAccessCollection {
    public typealias Element         = OutElement
    public typealias Index           = Int
    //public typealias SubSequence     = MapCollection
    public typealias Indices         = DefaultIndices<MapCollection>

    public subscript(position: Int) -> Self.Element {
        _read {
            yield block(data[position as! Data.Index], position)
        }
    }
    
    public var startIndex    : Int { 0 }
    public var endIndex      : Int { Swift.min(limit, data.endIndex as! Int) }
    
    public func index(before i: Int) -> Int { return i - 1 }
    public func index(after i: Int)  -> Int { return i + 1 }
}
