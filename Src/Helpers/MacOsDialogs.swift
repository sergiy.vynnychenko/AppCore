import Essentials
import SwiftUI

// !!!!!!!!!!!!!!!!!!!!!!
// There is no reason to change "title: String" to "LocalizedStringKey"!!!
// It's trying to use appCore localized strings in this case!
// Use call open(title: String(localized: "some val"), conf: []], startUrl: nil, submitBtnText: String(localized: "some val"))
// !!!!!!!!!!!!!!!!!!!!!!!

@available(macOS 12.0, *)
public class MacOsDialogs {
    public static func open(title: String, conf: Set<MacOsDlgSettings>, startUrl: URL?, submitBtnText: String? = nil) -> URL? {
        let dialog = NSOpenPanel().then {
            $0.title                   = String(localized: "\(title)")
            $0.message                 = String(localized: "\(title)")
            $0.canHide = false
            $0.showsResizeIndicator    = true
            $0.canCreateDirectories    = true
            
            if let startUrl = startUrl {
                $0.directoryURL = startUrl
            }
            
            $0.canChooseDirectories    = conf.contains(.canChooseDirs)
            $0.canChooseFiles          = conf.contains(.canChooseFiles)
            
            $0.showsHiddenFiles        = conf.contains(.showsHiddenFiles)
            
            if let submitBtnText = submitBtnText {
                $0.prompt = submitBtnText
            }
        }
        
        if (dialog.runModal() == NSApplication.ModalResponse.OK) {
            return dialog.url
        }
        
        return nil
    }
    
    public static func openMany(title: String, conf: Set<MacOsDlgSettings>, startUrl: URL?, submitBtnText: String? = nil) -> [URL]? {
        let dialog = NSOpenPanel().then {
            $0.title                   = String(localized: "\(title)")
            $0.message                 = String(localized: "\(title)")
            $0.canHide = false
            $0.showsResizeIndicator    = true
            $0.canCreateDirectories    = true
            
            if let startUrl = startUrl {
                $0.directoryURL = startUrl
            }
            
            $0.canChooseDirectories    = conf.contains(.canChooseDirs)
            $0.canChooseFiles          = conf.contains(.canChooseFiles)
            $0.allowsMultipleSelection = true
            
            $0.showsHiddenFiles        = conf.contains(.showsHiddenFiles)
            
            if let submitBtnText = submitBtnText {
                $0.prompt = submitBtnText
            }
        }
        
        if (dialog.runModal() == NSApplication.ModalResponse.OK) {
            return dialog.urls
        }
        
        return nil
    }
    
    public static func save(title: String, startDir: URL, fileName: String, _ conf: Set<MacOsSaveDlgSettings> ) -> URL?  {
        let dialog = NSSavePanel().then {
            $0.title                   = String(localized: "\(title)")
            $0.message                 = String(localized: "\(title)")
            $0.showsResizeIndicator    = true
            $0.canCreateDirectories    = true
            
            $0.showsHiddenFiles        = conf.contains(.showsHiddenFiles)
            
            $0.directoryURL = startDir
            $0.nameFieldStringValue = fileName
        }
        
        if (dialog.runModal() == NSApplication.ModalResponse.OK) {
            return dialog.url
        }
        
        return nil
    }
}

public enum MacOsDlgSettings {
    case canChooseDirs
    case canChooseFiles
    case showsHiddenFiles
}

public enum MacOsSaveDlgSettings {
    case showsHiddenFiles
}
