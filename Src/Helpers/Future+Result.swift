//
//  Future+Result.swift
//  AppCore
//
//  Created by loki on 28.09.2020.
//  Copyright © 2020 Loki. All rights reserved.
//

import Foundation
import AsyncNinja

public extension ExecutionContext {
    func futureFrom<T>(executor: Executor? = nil, _ block: @escaping (Self)-> Result<T, Error>) -> Future<T> {
        self.promise(executor: executor) { me, promise in
            promise.complete(block(self))
        }
    }
}

public func futureFrom<T>(executor: Executor = .main, _ block: @escaping ()-> Result<T, Error>) -> Future<T> {
    promise(executor: executor) { promise in
        promise.complete(block())
    }
}
