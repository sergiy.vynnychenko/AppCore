//
//  DirectoryChangesMonitor.swift
//  AppCore
//
//  Created by UKS on 07.04.2021.
//  Copyright © 2021 Loki. All rights reserved.
//

import Foundation

public extension URL {
    var isSandboxAccessible : Bool { access(self.path, R_OK) == 0 }
    
    var isSandboxAccessibleWrite : Bool { access(self.path, W_OK) == 0 }
}

public class DirectoryChangesMonitor {
    private let fileDescriptor: CInt
    private let source: DispatchSourceProtocol
    
    public init?( URL: URL, block: @escaping () -> Void ) {
        if !URL.isSandboxAccessible {
            return nil
        }
        
        FS.mkdir(URL.path)
        
        self.fileDescriptor = open(URL.path, O_EVTONLY)
        self.source = DispatchSource.makeFileSystemObjectSource(fileDescriptor: self.fileDescriptor, eventMask: .all, queue: DispatchQueue.global())
        self.source.setEventHandler {
            block()
        }
        self.source.resume()
    }
    
    deinit {
        self.source.cancel()
        close(fileDescriptor)
    }
}
