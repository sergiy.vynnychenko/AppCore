//
//  AppCore.Error.swift
//  AppCore
//
//  Created by loki on 21.03.2021.
//  Copyright © 2021 Loki. All rights reserved.
//

import Foundation


public enum AppCoreError : Swift.Error {
    case generic(info: String)
}

extension AppCoreError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .generic(let info):
            return "Error: \(info)"
        }
    }
}
