
import Foundation
import Essentials

@available(macOS 10.15, *)
public struct KeychainSet {
    let name : String
    let dic : KeychainDictionary
    
    public init(name: String, service: String) {
        self.name = name
        self.dic = .init(service: service)
    }
}

public extension KeychainSet {
    var key : String { dic.service + "." + name }
    
    var items : R<[TokenEntry]> {
        dic.lookup(key: key)
            .flatMapError { error in
                if error.localizedDescription.contains("-25300") {
                    return .success("[]")
                }
                
                return .failure(error)
            }
        | { $0.decodeFromJson(type: [TokenEntry].self) }
    }
    
    func append(token: TokenEntry) -> R<Void> {
        self.items
            | { $0.appending(token).asJson() }
            | { dic.addForced(key: key, value: $0) }
    }
    
    func delete(token: TokenEntry) -> R<Void> {
        self.items
            | { $0.filter { $0.id != token.id }.asJson() }
            | { dic.addForced(key: key, value: $0) }
    }
    
    func update(newTokens: [TokenEntry]) -> R<Void> {
        newTokens.asJson() | { dic.addForced(key: key, value: $0) }
    }
    
    func clean() -> R<Void> {
        dic.delete(key: key)
    }
}

public extension KeychainSet {
    struct TokenEntry : Decodable, Encodable, Identifiable, Equatable, Hashable {
        public var id: String { "\(user).\(token)" }
        
        public let user : String
        public let token : String
        public let refreshToken: String?
        public let expiresAt: Date?
        
        public init(user: String, token: String, refreshToken: String?, expiresAt: Date?) {
            self.user = user
            self.token = token
            self.refreshToken = refreshToken
            self.expiresAt = expiresAt
        }
    }
}

public extension KeychainSet.TokenEntry {
    var isRefreshable : Bool { refreshToken != nil && expiresAt != nil }
    func isExpired(delta: TimeInterval = -60) -> Bool {
        if let exp = expiresAt {
            return Date() > exp.addingTimeInterval(delta)
        } else {
            return false
        }
    }
}
