//
//  Keychain.swift
//  AppCore
//
//  Created by loki on 13.06.2021.
//  Copyright © 2021 Loki. All rights reserved.
//

import Foundation
import Essentials

@available(macOS 10.15, *)
public struct Keychain {
    public static func add(key: String) -> R<Void> {
        return .notImplemented
    }
    
    public static func addSafe(account: Keychain.Account) -> R<Void> {
        account.asAddQuery | { SecurityItem.add($0) }
    }
    
    public static func addForced(account: Keychain.Account) -> R<Void> {
        account.asAddQuery
            .flatMap { SecurityItem.add($0) }
            .ifError(code: Int(errSecDuplicateItem)) { account.asUpdateQuery | { SecurityItem.update($0, $1) } }
    }
    
    public static func update(account: Keychain.Account) -> R<Void> {
        account.asUpdateQuery | { SecurityItem.update($0, $1) }
    }
    
    public static func delete(url: String) -> R<Void> {
        SecurityItem.delete( deleteQuery(server: url) )
    }
    
    public static func delete(account: String, service: String)-> R<Void> {
        SecurityItem.delete( deleteGenericQuery(account: account, service: service) )
    }
    
    public static func delete(service: String)-> R<Void> {
        SecurityItem.delete( deleteGenericQuery(service: service) )
    }
    
    public static func lookup(url: String) -> R<Keychain.Account> {
        SecurityItem.copyMatching(lookupQuery(server: url))
            | { $0 as? [String: Any] }
            | { $0.asNonOptional }
            | { $0.asAccount }
    }
    
    public static func lookup(account: String, serivce: String) -> R<Keychain.Account> {
        SecurityItem.copyMatching(lookupGenericQuery(account: account, service: serivce))
            | { $0 as? [String: Any] }
            | { $0.asNonOptional("lookupGenericQuery") }
            | { $0.asAccount }
    }
    
    public static func all() -> R<[Keychain.Account]> {
        let servers =  SecurityItem.copyMatching(queryAll())
            | { $0 as? [[String: Any]] }
            | { $0.asNonOptional }
            | { $0 | { $0.asServerUrl } }
            | { Array(Set($0)) } // distinct
        
        return servers
            | { $0.flatMapCatch { Keychain.lookup(url: $0) } }
            | { $0.results }
    }
}

func lookupGenericQuery(account: String, service: String) -> [String: Any] {
    var query = [String: Any]()
    query[String(kSecClass)]        = kSecClassGenericPassword
    
    query[String(kSecAttrAccount)]  = account
    query[String(kSecAttrService)]  = service

    query[String(kSecReturnAttributes)] = kCFBooleanTrue
    query[String(kSecReturnData)]       = kCFBooleanTrue
    return query
}

func lookupQuery(server: String) -> [String: Any] {
    var query = [String: Any]()
    
    query[String(kSecClass)]            = kSecClassInternetPassword
    query[String(kSecAttrServer)]       = server
    
    query[String(kSecReturnAttributes)] = kCFBooleanTrue
    query[String(kSecReturnData)]       = kCFBooleanTrue
    
    //query[String(kSecMatchLimit)]       = kSecMatchLimitOne

    return query
}

func queryAll() -> [String: Any] {
    var query = [String: Any]()
    
    query[String(kSecClass)]            = kSecClassInternetPassword
    query[String(kSecReturnAttributes)] = kCFBooleanTrue
    query[String(kSecMatchLimit)]       = kSecMatchLimitAll

    return query
}



@available(macOS 10.15, *)
private extension Dictionary where Key == String {
    var asAccount : R<Keychain.Account> {
        
        if (self[String(kSecClass)] as? String) == String(kSecClassInternetPassword) {
            return asSskKeyAccount
        } else if (self[String(kSecClass)] as? String) == String(kSecClassGenericPassword) {
            return asGenericAccount
        }
        
        return .failure(WTF("unknown protocol"))
        
        //query[String(kSecClass)]        = kSecClassInternetPassword
        //query[String(kSecClass)]        = kSecClassGenericPassword
        
//        if let _user = (self[String(kSecAttrAccount)] as? String) {
//            if _user == "No user account" {
//                return .failure(WTF("No user account"))
//            }
//        }
//
//        if let protocol_str = self[String(kSecAttrProtocol)] as? String {
//            if let p = InternetProtocol(rawValue: protocol_str) {
//                switch p {
//                case .ssh:
//                    return asSshAccount
//                case .https:
//                    return asHttpsAcclount
//                }
//            }
//        }
        
        

        
    }
    
//    var asHttpsAcclount : R<Keychain.Account> {
//        let pass    = attr(kSecValueData, of: Data.self) | { $0.asString() }
//        let user    = attr(kSecAttrAccount, of: String.self)
//        let url     = attr(kSecAttrServer, of: String.self)
//
//        return combine(user, pass, url)
//            .map { .https(user: $0, pass: $1, url: $2) }
//    }
//
//    var asSshAccount : R<Keychain.Account> {
//        let data    = attr(kSecValueData, of: Data.self) | { $0.asString() } |  { $0.asJSONArray }
//        let url     = attr(kSecAttrServer, of: String.self)
//
//        return combine(data, url)
//            .map { .ssh(pubKey: $0[0], privKey: $0[1], url: $1) }
//    }
    var asSskKeyAccount : R<Keychain.Account> {
        let pass    = attr(kSecValueData, of: Data.self) | { $0.asString() }
        let key     = attr(kSecAttrServer, of: String.self)
        
        return combine(key, pass)
            .map { .sshPrivate(key: $0, pass: $1) }
    }
    
    var asGenericAccount : R<Keychain.Account> {
        let pass    = attr(kSecValueData, of: Data.self) | { $0.asString() }
        let account = attr(kSecAttrAccount, of: String.self)
        let service = attr(kSecAttrService, of: String.self)
        
        return combine(account, pass, service)
            .map { .generic(account: $0, pass: $1, service: $2) }
    }
    
    func attr<T>(_ key: CFString, of: T.Type) -> R<T> {
        (self[String(key)] as? T).asNonOptional
    }
        
    var asServerUrl : R<String> {
        (self[String(kSecAttrServer)] as? String).asNonOptional
    }
}

private func deleteQuery(server: String) -> [String: Any] {
    var query: [String: Any] = [:]
    query[String(kSecAttrServer)]   = server
    query[String(kSecClass)]        = kSecClassInternetPassword
    
    return query
}

private func deleteGenericQuery(account: String, service: String) -> [String: Any] {
    var query: [String: Any] = [:]
    query[String(kSecClass)]        = kSecClassGenericPassword
    
    query[String(kSecAttrAccount)]  = account
    query[String(kSecAttrService)]  = service
    
    return query
}

private func deleteGenericQuery(service: String) -> [String: Any] {
    var query: [String: Any] = [:]
    query[String(kSecClass)]        = kSecClassGenericPassword
    query[String(kSecAttrService)]  = service
    return query
}

