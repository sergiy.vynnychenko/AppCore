
import Foundation
import Essentials

@available(macOS 10.15, *)
public struct KeychainDictionary {
    let service : String
    init(service: String) {
        self.service = service
    }
    
    public func addSafe(key: String, value: String) -> R<Void> {
        Keychain.addSafe(account: .generic(account: key, pass: value, service: service))
    }
    
    public func addForced(key: String, value: String) -> R<Void> {
        Keychain.addForced(account: .generic(account: key, pass: value, service: service))
    }
    
    public func delete(key: String) -> R<Void> {
        Keychain.delete(account: key, service: service)
    }
    
    public func update(key: String, value: String) -> R<Void> {
        Keychain.update(account: .generic(account: key, pass: value, service: service))
    }
    
    public func lookup(key: String) -> R<String> {
        Keychain.lookup(account: key, serivce: service) | { $0.genericPassword }
    }
    
    public func all() -> R<[[String: Any]]> {
        service.asData()
            | { SecurityItem.copyMatching(queryAllGenericPass(service: $0)) }
            | { ($0 as? [[String: Any]]).asNonOptional }
    }
}


fileprivate func queryAllGenericPass(service: Data) -> [String: Any] {
    var query = [String: Any]()
    
    query[kSecClass]            = kSecClassGenericPassword
    query[kSecReturnAttributes] = kCFBooleanTrue
    query[kSecMatchLimit]       = kSecMatchLimitAll
//    query[kSecAttrService]           = service

    return query
}


//private func addQuery(account: Data, service: Data, value: Data) -> [String : Any] {
//    var query: [String: Any] = [:]
//    query[kSecClass]                 = kSecClassGenericPassword
//    query[kSecAttrAccount]           = account
//    query[kSecAttrService]           = service
//    query[kSecValueData]             = value
//    return query
//}
//
//private func deleteQuery(account: Data, service: Data) -> [String: Any] {
//    var query: [String: Any] = [:]
//    query[kSecClass]         = kSecClassGenericPassword
//    query[kSecAttrAccount]   = account
//    query[kSecAttrService]   = service
//    return query
//}
//
//
//private func updateQuery(account: Data) -> [String : Any] {
//    var query: [String: Any] = [:]
//    query[kSecClass]          = kSecClassGenericPassword
//    query[kSecAttrAccount]    = account
//    return query
//}
//
//private func updateQuery(value: Data) -> [String : Any] {
//    var query: [String: Any] = [:]
//    query[kSecValueData]             = value
//    return query
//}
//
//private func lookupQuery(account: Data) -> [String : Any] {
//    var query: [String: Any] = [:]
//    query[kSecClass]                = kSecClassGenericPassword
//    query[kSecAttrAccount]          = account
//    query[kSecMatchLimit]           = kSecMatchLimitOne
//    query[kSecReturnData]           = kCFBooleanTrue
//    query[kSecReturnAttributes]     = kCFBooleanTrue
//    return query
//}
//
