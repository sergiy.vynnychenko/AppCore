//
//  Account.swift
//  AppCore
//
//  Created by loki on 13.06.2021.
//  Copyright © 2021 Loki. All rights reserved.
//

import Foundation
import Essentials

@available(macOS 10.15, *)
public extension Keychain {
    enum Account {
        case generic(account: String, pass: String, service: String)
        case sshPrivate(key: String, pass: String)
        
        var genericPassword : R<String> {
            switch self {
            case let .generic(_, pass, _): return .success(pass)
            default: return .wtf("not a generic pass")
            }
        }
    }
}

let SSH_USER = "ssh_user"
let SSH_PRIVATE_KEY_USER = "ssh_private_key_user"

@available(macOS 10.15, *)
extension Keychain.Account {
    var asAddQuery: R<[String: Any]> {
        switch self {
//        case let .https(user, pass, url):
//
//            return pass.asData()
//                | { addQuery(data: $0, user: user, server: url, protocol: .https) }
//
//        case let .ssh(pubKey, privKey, url):
//            return [pubKey, privKey]
//                .asJSONString
//                | { $0.asData() }
//                | { addQuery(data: $0, user: SSH_USER, server: url, protocol: .ssh) }
        case let .generic(account, pass, service):
            return pass.asData()
                | { addGenericQuery(data: $0, account: account, service: service) }
            
        case let .sshPrivate(key, pass):
            return pass.asData()
                | { addQuery(data: $0, user: SSH_PRIVATE_KEY_USER, server: key, protocol: .ssh) }
        }
    }
    
    var asUpdateQuery: R<([String: Any],[String: Any])> {
        switch self {
//        case let .https(user, pass, url):
//            return pass.asData()
//                | { attributesToUpdate(data: $0, user: user)}
//                | { (updateQuery(server: url), $0)}
//
//        case let .ssh(pubKey, privKey, url):
//            return [pubKey, privKey]
//                .asJSONString
//                | { $0.asData() }
//                | { attributesToUpdate(data: $0, user: SSH_USER)}
//                | { (updateQuery(server: url), $0)}
        case let .generic(account, pass, service):
            return pass.asData()
                | { (updateGenericQuery(account: account, service: service), attributesToUpdate(data: $0))}
            
        case let .sshPrivate(key, pass):
            return pass.asData()
                | { attributesToUpdate(data: $0, user: SSH_PRIVATE_KEY_USER)}
                | { (updateQuery(server: key), $0)}
        }
    }
    
    var asDeleteQuery: [String: Any] {
        let user : String
        let server : String
        
        switch self {
//        case let .https(_user, _, url):
//            user = _user
//            server = url
//        case let .ssh(_, _, url):
//            user = "ssh_user"
//            server = url
        case let .sshPrivate(key, _):
            user = "ssh_private_key_user"
            server = key
            
        case let .generic(account, _, service):
            user = account
            server = service
        }
        
        
        var query: [String: Any] = [:]
        query[String(kSecAttrAccount)]  = user
        query[String(kSecAttrServer)]   = server
        query[String(kSecClass)] = kSecClassInternetPassword
        
        return query
    }
}

private func addGenericQuery(data: Data, account: String, service: String) -> [String: Any] {
    var query: [String: Any] = [:]
    query[String(kSecClass)]        = kSecClassGenericPassword
    
    query[String(kSecValueData)]    = data
    query[String(kSecAttrAccount)]  = account
    query[String(kSecAttrService)]  = service
    
    return query
}


extension Dictionary where Key == String, Value == Any {
    subscript(key: CFString) -> Any? {
        get {
            self[String(key)]
        }
        set(value) {
            self[String(key)] = value
        }
    }
}




private func addQuery(data: Data, user: String, server: String, protocol: InternetProtocol) -> [String: Any] {
    var query: [String: Any] = [:]
    
    query[String(kSecValueData)]    = data
    query[String(kSecAttrAccount)]  = user
    
    query[String(kSecClass)]        = kSecClassInternetPassword
    query[String(kSecAttrServer)]   = server
    query[String(kSecAttrProtocol)] = `protocol`.rawValue
    
    return query
}

private func updateQuery(server: String) -> [String: Any] {
    var query: [String: Any] = [:]
    query[String(kSecClass)]        = kSecClassInternetPassword
    query[String(kSecAttrServer)]   = server
    return query
}

private func updateGenericQuery(account: String, service: String) -> [String: Any] {
    var query: [String: Any] = [:]
    query[String(kSecClass)]        = kSecClassGenericPassword
    query[String(kSecAttrAccount)]  = account
    query[String(kSecAttrService)]  = service
    return query
}

private func attributesToUpdate(data: Data, user: String) -> [String: Any] {
    var query: [String: Any] = [:]
    
    query[String(kSecValueData)]    = data
    query[String(kSecAttrAccount)]  = user
    
    return query
}


private func attributesToUpdate(data: Data) -> [String: Any] {
    var query: [String: Any] = [:]
    query[String(kSecValueData)]    = data
    return query
}
