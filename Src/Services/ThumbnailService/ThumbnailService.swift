//
//  ThumbnailService.swift
//  TaoGit
//
//  Created by Loki on 4/2/19.
//  Copyright © 2019 Cheka Zuja. All rights reserved.
//

import Foundation

public class ThumbnailService { // bla
    public let url : URL
    
    public init(folder: String = "") {
        url = FSApp.appFolder().appendingPathComponent("Thumbnails").appendingPathComponent(folder)
        url.makeSureDirExist()
            .onFailure { assert(false, $0.localizedDescription) }
    }
    
    public func add( url: URL) -> String? {
        guard url.isFileURL else { return nil }
        
        var file = url
        let ext = "." + file.pathExtension
        file.deletePathExtension()
        
        let dst = self.url.appendingPathComponent(file.lastPathComponent + UUID().uuidString + ext)
        
        AppCore.log(title: "ThumbnailService", msg: dst.path)
        AppCore.log(title: "ThumbnailService", msg: dst.lastPathComponent)
        
        let folder = dst.deletingLastPathComponent()
        folder.makeSureDirExist()
            .onFailure { assert(false, $0.localizedDescription) }
        
        //TODO: possibly need to do sth with this
        _ = FS.copy(url, toUrl: dst)
        
        return dst.lastPathComponent
    }
    
    public func replace(file: String, with url: URL) -> String? {
        if file.count > 0 {
            _ = self.url.appendingPathComponent(file).FS.delete()
        }
        return add(url: url)
    }
    
}
