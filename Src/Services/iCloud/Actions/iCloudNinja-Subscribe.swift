//
//  iCloudNinja-Subscribe.swift
//  AppCore
//
//  Created by Loki on 7/10/19.
//  Copyright © 2019 Loki. All rights reserved.
//

import CloudKit
import AsyncNinja

public extension CKDatabase {
    func subscribe(_ subscription: CKSubscription) -> Future<CKSubscription> {
        return promise() { promise in
            self.save(subscription) { subscription, error in
                if let subscription = subscription {
                    log(msg: "subscribed to \(subscription.subscriptionID)")
                    promise.succeed(subscription)
                }
                
                if let error = error {
                    log(error: error)
                    promise.fail(error)
                }
            }
        }
    }
    
    func fetchAllSubscriptions() -> Future<[CKSubscription]> {
        return promise() { promise in
            self.fetchAllSubscriptions { subscriptions, error in
                if let subscriptions = subscriptions {
                    log(msg: "fetched \(subscriptions.count) subscriptions")
                    promise.succeed(subscriptions)
                }
                
                if let error = error {
                    log(error: error)
                    promise.fail(error)
                }
            }
        }
    }
}

public enum SubscriptionStatus {
    case fetched([CKSubscription])
    case subscribed(CKSubscription)
    
    public var description : String {
        switch self {
        case .fetched(let ss): return "SubscriptionStatus.fetched \(ss.map { $0.subscriptionID })"
        case .subscribed(let s): return "SubscriptionStatus.subscribed \(s.subscriptionID)"
        }
    }
}

public extension Future where Success == [CKSubscription] {
    func subscribeIfEmpty(on db: CKDatabase, subscription: CKSubscription) -> Future<SubscriptionStatus> {
        self.flatMap { value in
            if value.isEmpty {
                return db.subscribe(subscription).map { .subscribed($0) }
            } else {
                return future(success: .fetched(value))
            }
        }
    }
}

fileprivate func log(error: Error) {
    AppCore.log(title: "iCloudNinja", error: error)
}

fileprivate func log(msg: String) {
    AppCore.log(title: "iCloudNinja", msg: msg, thread: true)
}
