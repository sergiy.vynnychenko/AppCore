//
//  iCloudNinjaService.swift
//  SwiftCore
//
//  Created by Loki on 1/10/19.
//  Copyright © 2019 checkazuja. All rights reserved.
//

import Foundation
import AsyncNinja
import CloudKit
import Essentials

fileprivate let publicDB  = CKContainer.default().publicCloudDatabase
fileprivate let privateDB = CKContainer.default().privateCloudDatabase

public class iCloundNinjaPrivate : iCloudNinjaService {
    init() {
        super.init(container: CKContainer.default(), cloudDB: privateDB)
    }
}

public class iCloundNinjaPublic : iCloudNinjaService {
    init() {
        super.init(container: CKContainer.default(), cloudDB: publicDB)
    }
}



public class iCloudNinjaService : ExecutionContext, ReleasePoolOwner, AppLogger {
    public let executor: Executor = Executor(queue: DispatchQueue(label: "iCloudQueue"))
    public let releasePool = ReleasePool()
    
    public let container  : CKContainer
    public let cloudDB    : CKDatabase
    
    public var batchSize  = 2
    
    public init(container: CKContainer, cloudDB: CKDatabase) {
        self.container  = container
        self.cloudDB    = cloudDB
    }
    
    public func waitForAuth(retryAfterSeconds: UInt32 = 30) -> Future<Void> {
        return future(context: self, executor: .default) { ctx in
            while ctx.status() != .available {
                sleep(retryAfterSeconds)
            }
        }
    }
    
    private func status() -> CKAccountStatus {
        let status = container.status().wait()
        return status.maybeSuccess ?? .couldNotDetermine
    }
    
    public func push(records: Channel<[CKRecord],Void>) -> Channel<[CKRecord], Void> {
        return records
            .flatMap(context: self) { $0.cloudDB.push(records: $1) }
    }
  
    public func push(records: [CKRecord]) -> Channel<([String],[String]), Void> {
        return records.chunked(by: batchSize)
            .flatMapToChannel(context: self) { me, chunk in
                me.cloudDB
                    .push(records: chunk)
                    .map(context: me) { _, pushed in
                        (pushed.map { $0.recordID.recordName }, chunk.map { $0.recordID.recordName })
                    }
            }
    }
    
    public func fetch(IDs: [CKRecord.ID]) -> Channel<[CKRecord.ID:CKRecord], Void> {
        return IDs.chunked(by: batchSize)
            .flatMapToChannel(context: self) { me, chunk in
                me.cloudDB.fetch(IDs: chunk)
            }
    }
    
//    public func fetchChangeWith(token: CKServerChangeToken?) -> Channel<CKQueryNotification, CKServerChangeToken> {
//        return container.fetch(token: token)
//    }
    
    public func fetch(query: CKQuery) -> Channel<[CKRecord], Void> {
        return cursor(context: self, cursor: CKQueryOperation(query: query)) { me, cursor in
            me.cloudDB.query(operation: cursor)
        }
    }
    
    public func fetchRecordsOf(type: String, predicate: NSPredicate? = nil) -> Channel<[CKRecord], Void> {
        let queryAll = CKQuery(recordType: type, predicate: predicate ?? NSPredicate(value: true))
        return fetch(query: queryAll)
    }
    
    public func delete(IDs: Channel<[CKRecord.ID], Void>, skipErrors: Bool = false) -> Channel<[CKRecord.ID], Void> {
        return IDs
            .flatMap(context: self) { me, ids in
                me.cloudDB.delete(IDs: ids).asChannel(executor: me.executor)
        }
    }
    
    public func deleteRecordsOf(type: String, skipErrors: Bool = false) -> Channel<[CKRecord.ID], Void> {
        let ids = fetchRecordsOf(type: type).map { $0.map { $0.recordID } }
        return delete(IDs: ids, skipErrors: skipErrors)
    }
    
    public func subscribe(to subscription: CKSubscription) -> Future<CKSubscription> {
        return cloudDB.subscribe(subscription)
    }
    
    public func fetchAllSubscriptions() -> Future<[CKSubscription]> {
        return cloudDB.fetchAllSubscriptions()
    }
    
    public func userRecordID() -> Future<CKRecord.ID> {
        return container.userRecordID()
    }
}

fileprivate func log(msg: String) {
    AppCore.log(title: "iCloudNinja", msg: msg, thread: true)
}
