
import Foundation
import Realm
import RealmSwift
import Essentials

public extension Realm {
    static func open(config: Realm.Configuration) -> R<Realm> {
        do {
            return .success(try Realm(configuration: config))
        } catch let error {
            return .failure(error)
        }
    }
    
    func writeCopy(url: URL, key: Data) -> R<Void> {
        do {
            try self.writeCopy(toFile: url, encryptionKey: key)
            return .success(())
        } catch let error {
            return .failure(error)
        }
    }
}
