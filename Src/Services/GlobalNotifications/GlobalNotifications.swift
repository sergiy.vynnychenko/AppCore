//
//  GlobalNotifications.swift
//  AppCore
//
//  Created by loki on 07.11.2020.
//  Copyright © 2020 Loki. All rights reserved.
//

import Foundation
import AsyncNinja

let hostAppId = Bundle.main.bundleIdentifier
let configPropertyDidSet  = Notification.Name("AppCore.ConfigPropertyDidSet")

public func sendGlobal(notification name: Notification.Name, userInfo: [AnyHashable : Any]? = nil) {
    DistributedNotificationCenter.default()
        .postNotificationName(name, object: hostAppId, userInfo: userInfo, options: .deliverImmediately)
}

public func subscribeForGlobal(notification name: Notification.Name) -> Channel<Notification, Void> {
    let producer = Producer<Notification, Void>()
    
    DistributedNotificationCenter.default
        .addObserver(forName: name, object: hostAppId, queue: nil) { notification in
            producer.update(notification)
        }
    return producer
}




