//
//  OptionSet+Ext.swift
//  AppCore
//
//  Created by Serhii Vynnychenko on 3/2/20.
//  Copyright © 2020 Loki. All rights reserved.
//

import Foundation

public extension OptionSet where RawValue == UInt32 {
    func with( _ options: Self, on: Bool) -> Self {
        if on {
            return Self(rawValue: self.rawValue | options.rawValue)
        } else {
            return Self(rawValue: self.rawValue & ~options.rawValue)
        }
    }
}
