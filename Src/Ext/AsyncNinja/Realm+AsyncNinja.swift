//
//  Realm+AsyncNinja.swift
//  AppCore
//
//  Created by loki on 19.07.2020.
//  Copyright © 2020 Loki. All rights reserved.
//

import Foundation
import Realm
import RealmSwift
import AsyncNinja

public extension Realm {
    func notifications(executor: Executor = .main) -> Channel<Realm.Notification, Void> {
        return producer(executor: executor) { producer in
            let token = self.observe { event, realm in
                producer.update(event)
            }
            
            producer._asyncNinja_retainUntilFinalization(token)
        }
    }
}

public extension Results {
    /// use of keypaths: ["orders.status"]
    func notifications(executor: Executor = .main, keyPaths: [String]? = nil) -> Channel<RealmCollectionChange<Results<Element>>, Void> {
        return producer(executor: executor) { producer in
            let token = self.observe(keyPaths: keyPaths) { event in
                producer.update(event)
            }
            
            producer._asyncNinja_retainUntilFinalization(token)
        }
    }
}
