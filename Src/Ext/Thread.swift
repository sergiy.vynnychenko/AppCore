//
//  Thread.swift
//  AppCore
//
//  Created by UKS on 26.04.2021.
//  Copyright © 2021 Loki. All rights reserved.
//

import Foundation

public func sleep(ms: Int ) {
    usleep(useconds_t(ms * 1000))
}

public func sleep(sec: Double ) {
    usleep(useconds_t(sec * 1_000_000))
}
