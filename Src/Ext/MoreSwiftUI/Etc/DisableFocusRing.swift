//
//  DisableFocusRing.swift
//  AppCore
//
//  Created by UKS on 25.05.2021.
//  Copyright © 2021 Loki. All rights reserved.
//

import SwiftUI

//Disable focusRing for any TextField in project
public extension NSTextField {
    override var focusRingType: NSFocusRingType {
            get { .none }
            set { }
    }
}
