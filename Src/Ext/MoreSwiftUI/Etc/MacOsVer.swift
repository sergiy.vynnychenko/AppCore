import Foundation

public class MacOsVer {
    public static var higherThan_11: Bool {
        if #available(macOS 11.0, *) { return true }
        else { return false }
    }
    
    public static var higherThan_11_3: Bool {
        if #available(macOS 11.3, *) { return true }
        else { return false }
    }
    
    public static var higherThan_12: Bool {
        if #available(macOS 12, *) { return true }
        else { return false }
    }
}
