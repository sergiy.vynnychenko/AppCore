import Foundation
import Cocoa
import SwiftUI
import AppKit

public struct TableSpec {
    let heightOfRow : CGFloat
    let intercellSpacing: NSSize
    let topMargin : CGFloat
    let bottomMargin : CGFloat
    let bottomMarginEx : CGFloat
    
    let multiSelect : Bool
    
    let extraRows : Int
    let idxOffset : Int
    let firstResponder : Bool
    let highlightStyle: NSTableView.SelectionHighlightStyle
    
    
    public init(heightOfRow : CGFloat = 20, cellSpacing: NSSize = .zero, topMargin: CGFloat = 0, bottomMargin: CGFloat = 0, bottomMarginEx: CGFloat = 0, multiSelect: Bool = true, firstResponder: Bool = false, highlightStyle: NSTableView.SelectionHighlightStyle = .regular) {
        self.topMargin = topMargin
        self.bottomMargin = bottomMargin
        self.bottomMarginEx = bottomMarginEx
        self.heightOfRow = heightOfRow
        self.intercellSpacing = cellSpacing
        self.multiSelect = multiSelect
        
        var extra = (topMargin == 0) ? 0 : 1
        extra += (bottomMargin == 0) ? 0 : 1
        self.extraRows = extra
        
        self.idxOffset = (topMargin > 0) ? 1 : 0
        self.firstResponder = firstResponder
        self.highlightStyle = highlightStyle
    }
}

internal extension TableSpec {
    var shouldResizeScroll : Bool { topMargin > 0 || bottomMargin > 0}
}

@available(macOS 11.0, *)
public class NSTableController<T: RandomAccessCollection, RowView: View>: NSViewController, NSTableViewDelegate, NSTableViewDataSource where T.Index == Int {
    let         id : String
    var         factory: (T.Element, Int) -> RowView
    weak var    tableView: NSTableView?
    let         selection : Binding<Set<Int>>?

    
    var items : CollectionMapper<T>
    
    public init(id: String, collection: T, spec : TableSpec, selection : Binding<Set<Int>>?, @ViewBuilder factory: @escaping (T.Element, Int) -> RowView) {
        self.id = id
        self.items = CollectionMapper(collection: collection, spec: spec)
        self.factory = factory
        self.selection = selection
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func viewDidLayout() {
        guard let tableView = tableView else { return }
        tableView.sizeLastColumnToFit()
        
        
        let spec = items.spec
        
        if spec.shouldResizeScroll,     let scrollView = self.view as? NSScrollView {
            if let vscroll = scrollView.verticalScroller {
                
                var origin = vscroll.frame.origin
                var size   = vscroll.frame.size
                
                origin.y += spec.topMargin
                let newHeight = size.height - (spec.topMargin + spec.bottomMargin)
                
                size.height = max(newHeight, spec.heightOfRow * 2)
                
                vscroll.frame = NSRect(origin: origin, size: size)
            }
        }
    }

    // MARK: - NSTableViewDataSource protocol
    public func numberOfRows(in tableView: NSTableView) -> Int {
        items.countTotal
    }
    
//    public func selectionShouldChange(in tableView: NSTableView) -> Bool {
//        !(selection == nil)
//    }
    
    public func tableView(_ tableView: NSTableView, shouldSelectRow row: Int) -> Bool {
        guard selection != nil else { return false }
        
        return items[row] != nil
    }
    
    var shouldNotifySelectionDidChange = true
    public func tableViewSelectionDidChange(_ notification: Notification) {
        guard shouldNotifySelectionDidChange else { return }
        
        guard let indexes = self.tableView?.selectedRowIndexes else { return }

        let sel = items.externalSelection(from: indexes)
        DispatchQueue.main.async {
            self.selection?.wrappedValue = sel
        }
        
        TableStates.update(selection: sel, id: id)

        AppCore.log(title: "AppCore.Table.\(id)", msg: "tableViewSelectionDidChange \(sel) \(indexes.map { $0 } )")
    }
    
    func set(selection indexes: Set<Int>) {
        tableView?.selectRowIndexes(items.internalSelection(from: indexes), byExtendingSelection: false)
    }

    
    public func tableView(_ tableView: NSTableView, heightOfRow row: Int) -> CGFloat {
        items.height(row: row)
    }
    
    public func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
        let targetRow = items.topSpacerIdx != nil ? row - 1: row
        
        if let item = items[row] {
            return NSHostingView(rootView: factory(item, targetRow))
        }

        return nil
    }
    
    func makeFirstResponder() {
        if tableView?.window == NSApplication.shared.mainWindow {
            NSApplication.shared.mainWindow?.makeFirstResponder(self)
        }
    }
}

@available(OSX 11.0, *)
public struct Table<T: RandomAccessCollection, RowView: View>: NSViewControllerRepresentable where T.Index == Int {
    public typealias NSViewControllerType = NSTableController

    let id : String
    @ViewBuilder var factory: (T.Element,Int) -> RowView
    let collection : T
    let selection : Binding<Set<Int>>?
    let spec : TableSpec
    
    public init(id: String, _ collection: T, heightOfRow : CGFloat = 20, selection : Binding<Set<Int>>? = nil, @ViewBuilder factory: @escaping (T.Element, Int) -> RowView) {
        self.id = id
        self.collection = collection
        self.spec = TableSpec(heightOfRow: heightOfRow, topMargin: 0, bottomMargin: 0)
        self.factory = factory
        self.selection = selection
    }
    
    public init(id: String, _ collection: T, spec: TableSpec, selection : Binding<Set<Int>>? = nil, @ViewBuilder factory: @escaping (T.Element, Int) -> RowView) {
        self.id = id
        self.collection = collection
        self.spec = spec
        self.factory = factory
        self.selection = selection
    }

    public func makeNSViewController(context: Context) -> NSTableController<T, RowView> {
        let controller = NSTableController(id: id, collection: collection, spec: spec, selection: selection, factory: factory)
        let table = NSTableView()
        
        let column = NSTableColumn(identifier: NSUserInterfaceItemIdentifier(rawValue: "Column"))
        column.headerCell.title = "WTF"
        table.addTableColumn(column)
        table.columnAutoresizingStyle = .uniformColumnAutoresizingStyle
        table.headerView = nil
        table.allowsMultipleSelection = spec.multiSelect
        table.style = .plain
        table.intercellSpacing = .zero
        table.selectionHighlightStyle = spec.highlightStyle;
        table.intercellSpacing = spec.intercellSpacing
//        table.intercellSpacing
        

        controller.tableView = table
        controller.view = scrollView(content: table)
        
        table.delegate = controller
        table.dataSource = controller
        
        if let selection = selection?.wrappedValue {
            controller.set(selection: selection)
        }
        
        if spec.firstResponder {
            // delayed calls in main thread
            DispatchQueue.main.async {
                controller.makeFirstResponder()
                
                if let selection = selection?.wrappedValue {
                    DispatchQueue.global(qos: .utility).async {
                        sleep(sec: 0.2)
                        
                        DispatchQueue.main.async {
                            controller.set(selection: selection)
                        }
                    }
                }
            }
        }
        
        return controller
    }
    


    public func updateNSViewController(_ nsViewController: NSTableController<T, RowView>, context: Context) {
        guard let tableView = nsViewController.tableView else { return }
        nsViewController.items.collection = collection
        nsViewController.items.spec = spec
        
//        let rect = tableView.visibleRect
//        let rows = tableView.rows(in: rect)
//        let indexes = IndexSet(integersIn: Range(rows)!)
//
//        tableView.reloadData(forRowIndexes: indexes, columnIndexes: IndexSet())
        nsViewController.shouldNotifySelectionDidChange = false
        let selection = tableView.selectedRowIndexes
        tableView.reloadData()
        tableView.selectRowIndexes(selection, byExtendingSelection: false)
//        if let selection = selection?.wrappedValue {
//            nsViewController.set(selection: selection)
//        }
        nsViewController.shouldNotifySelectionDidChange = true
    }

}

fileprivate func scrollView(content: NSView) -> NSScrollView {
    let scrollView = NSScrollView(frame: .zero)
    scrollView.hasVerticalScroller = true
    //scrollView.autohidesScrollers = false
    scrollView.documentView = content
    return scrollView
}

struct CollectionMapper<T: RandomAccessCollection> where T.Index == Int {
    var collection : T
    var spec : TableSpec
    
    var countTotal  : Int  { collection.count + spec.extraRows }
    var topSpacerIdx    : Int? { spec.topMargin == 0 ? nil : 0 }
    var bottomSpacerIdx : Int? {
        guard spec.bottomMargin > 0 else { return nil }
        return countTotal - 1
    }
    
    public subscript(position: Int) -> T.Element? {
        guard position >= 0 else { return nil }
        
        let idx : Int
        if spec.topMargin > 0 {
            idx = position - 1
        } else {
            idx = position
        }
        
        guard idx >= 0 else { return nil }
        guard idx < collection.count else { return nil }
        
        return collection[idx]
    }
    
    func height(row: Int) -> CGFloat {
        if row == topSpacerIdx      { return spec.topMargin }
        if row == bottomSpacerIdx   { return max(spec.bottomMargin, spec.bottomMarginEx) }

        return spec.heightOfRow
    }
    
    func externalSelection(from indexes: IndexSet) -> Set<Int> {
        if spec.topMargin > 0 {
            return Set(indexes.map { $0 - 1 })
        } else {
            return Set(indexes)
        }
    }
    
    func internalSelection(from indexes: Set<Int>) -> IndexSet {
        var spacers = Set<Int>()
        if let idx = topSpacerIdx {
            spacers.insert(idx)
        }
        if let idx = bottomSpacerIdx {
            spacers.insert(idx)
        }
        
        if spec.topMargin > 0 {
            return IndexSet(indexes.filter{ ($0 < collection.count) && !spacers.contains($0) }.map { $0 + 1 })
        } else {
            return IndexSet(indexes.filter{ ($0 < collection.count) && !spacers.contains($0) })
        }
    }
}
