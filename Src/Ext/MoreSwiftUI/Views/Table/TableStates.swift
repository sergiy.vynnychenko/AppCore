
import Foundation
import Cocoa
import SwiftUI

@available(macOS 10.15, *)
public class TableStates : NinjaContext.Main, ObservableObject {
    @Published public var selection : Set<Int>
    
    private static let prefix = "AppCore.Table.Selection."
    
    public init(id: String) {
        let selectionID = TableStates.prefix + id
        
        selection = AppCore.states.valueFor(key: selectionID) ?? []
        
        super.init()
        
        AppCore.states.subscribeFor(key: selectionID, valueOfType: Set<Int>.self)
            .skipFirst()
            .assign(on: self, to: \.selection)
    }
    
    static func update(selection: Set<Int>, id: String) {
        AppCore.states.set(value: selection, forKey: TableStates.prefix + id)
    }
}
