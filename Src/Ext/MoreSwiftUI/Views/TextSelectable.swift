//
//  TextSelectable.swift.swift
//  AppCore
//
//  Created by UKS on 20.12.2021.
//  Copyright © 2021 Loki. All rights reserved.
//

import Foundation
import SwiftUI

@available(macOS 10.15.0, *)
public struct TextSelectable: View {
    var text : String
    
    public init(_ text: String) {
        self.text = text
    }
    
    public var body: some View {
        if #available(macOS 12.0, *) {
            Text(text)
                .textSelection(.enabled)
        } else {
            Text(text)
                .contextMenu{
                    Button("Copy") {
                        Clipboard.set(text: text)
                    }
                }
        }
    }
}
