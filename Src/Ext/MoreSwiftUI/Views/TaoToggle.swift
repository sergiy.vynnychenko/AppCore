//
//  TaoToggle.swift
//  TaoGit
//
//  Created by Loki on 14.11.2019.
//  Copyright © 2019 Cheka Zuja. All rights reserved.
//

import SwiftUI

@available(OSX 11.0.0, *)
public struct TaoToggle : View {
    public var state : CheckState = CheckState.mixed
    public var action : () -> Void = {}
    @State public var hover : Bool = false
    
    @State private var rotating: Bool = false
    
    public init( state: CheckState, action: @escaping () -> Void ){
        self.state  = state
        self.action = action
    }
    
    public var body : some View {
        ZStack {
            ZStack {
                if state == .busy {
                    Rectangle().theme(.normal(.lvl_1))
                } else if hover {
                    Rectangle().theme(.normal(.lvl_6))
                } else {
                    Rectangle().theme(.normal(.lvl_2))
                }
                
                Text(state.str).foregroundColor(.white)
            }
            .frame(width:18, height: 18)
            .cornerRadius(5)
            .onTapGesture {
                if state != .busy {
                    self.action()
                }
            }
            .onHover { self.hover = $0 }
            .animation(.easeOut(duration: 0.3), value: hover)
            
            if (state == .busy) {
                Image(systemName: "circle.dashed")
                    .resizable()
                    .scaledToFit()
                    .frame(width:18, height: 18)
                    .theme(.normal(.lvl_6))
                    .rotationEffect(.degrees(rotating ? 360 : 0))
                    .animation(.easeInOut(duration: 1).repeatForever(), value: rotating)
                    .onAppear {
                        rotating.toggle()
                    }
            }
        }.animation(.easeInOut(duration: 0.3), value: state)
    }
}

@available(OSX 11.0.0, *)
public extension TaoToggle {
    enum CheckState {
        case on
        case off
        case mixed
        case busy
        
        public var str : String {
            switch self {
            case .on:       return "✓"
            case .off:      return " "
            case .mixed:    return "■"
            case .busy:     return ""
            }
        }
        
        public var strStar : String {
            
            
            switch self {
            case .on:       return "★"
            case .off:      return "★"
            case .mixed:    return "★"
            case .busy:     return ""
            }
        }
        
        public mutating func toggle() {
            switch self {
            case .on:    self = .off
            case .off:   self = .on
            case .mixed: self = .on // redefine if necessary and remove this comment
            case .busy:  self = .on // redefine if necessary and remove this comment
            }
        }
    }
}
