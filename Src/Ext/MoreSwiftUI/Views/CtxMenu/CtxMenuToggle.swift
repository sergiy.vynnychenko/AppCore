//
//  CtxMenuToggle.swift
//  AppCore
//
//  Created by UKS on 09.08.2021.
//  Copyright © 2021 Loki. All rights reserved.
//

import SwiftUI

@available(OSX 11.0, *)
public struct CtxMenuToggle: View {
    @ObservedObject var isOn : ConfigProperty<Bool>
    //let isOn: Binding<Bool>
    let lbl: String
    
    public init(isOn: ConfigProperty<Bool>, lbl: String) {
        self.isOn = isOn
        self.lbl = lbl
    }
    
    public var body: some View {
        Button(action: { isOn.value.toggle() } ) {
            HStack(spacing: 0) {
                Text(isOn.value ? "✓" : "")
                +
                Text("\t\(lbl)")
            }
        }
    }

}
