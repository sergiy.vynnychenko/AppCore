//  AttributedText.swift
//  AppCore
//
//  Created by Loki on 28.02.2020.
//  Copyright © 2020 Loki. All rights reserved.
//

import SwiftUI
import Cocoa
import AsyncNinja


@available(OSX 11.0, *)
public struct TextFieldCustom: NSViewRepresentable {
    @Binding var text: String
    @Binding var enabled: Bool
    var action: () -> ()
    

    public init(string: Binding<String>, enabled: Binding<Bool>, action: @escaping ()->() ) {
        _text = string
        _enabled = enabled
        self.action = action 
    }
    
    let enterKeyCode = 36
    let escKeyCode = 53
    
    public func makeNSView(context: Context) -> NSTextField {
        let textField = NSTextField(string: text)
        textField.delegate = context.coordinator
        textField.isEditable = enabled
        textField.isBordered = false
        textField.backgroundColor = nil
        textField.focusRingType = .none
        
        let monitor = NSEvent.localMonitor(matching: .keyUp)
           
        monitor
            .filter(context: textField )   { _, event in event.keyCode == escKeyCode }
            .onUpdate(context: textField ) { _, event in self.enabled.toggle(); print("ESC") }
        
        monitor
            .filter(context: textField )   { _, event in event.keyCode == enterKeyCode }
            .onUpdate(context: textField ) { _, event in self.enabled.toggle(); action(); print("ENTER") }
        
        return textField
    }
    
    public func makeCoordinator() -> Coordinator {
        Coordinator { self.text = $0 }
    }
    
    public func updateNSView(_ nsView: NSTextField, context: Context) {
        nsView.stringValue = text
        nsView.isEditable = enabled
    }
}
