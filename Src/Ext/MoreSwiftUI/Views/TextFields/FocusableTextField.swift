//
//  TextFieldExtended.swift
//  AppCore
//
//  Created by UKS on 28.07.2021.
//  Copyright © 2021 Loki. All rights reserved.
//

import Foundation
import SwiftUI

@available(OSX 11.0, *)
public struct FocusableTextField: NSViewRepresentable {
    @Binding var stringValue: String
    var placeholder: String
    var autoFocus = false
    var tag: Int = 0
    var focusTag: Binding<Int>?
    var onChange: (() -> Void)?
    var onCommit: (() -> Void)?
    var onTabKeystroke: (() -> Void)?
    @State private var didFocus = false
    
    public init( stringValue: Binding<String>,
                 placeholder: String,
                 autoFocus: Bool = false,
                 tag: Int = 0,
                 focusTag: Binding<Int>?,
                 onChange: (() -> Void)? = nil,
                 onCommit: (() -> Void)? = nil,
                 onTabKeystroke: (() -> Void)? = nil
    ) {
        self._stringValue = stringValue
        self.placeholder = placeholder
        self.autoFocus = autoFocus
        self.tag = tag
        self.focusTag = focusTag
        self.onChange = onChange
        self.onCommit = onCommit
        self.onTabKeystroke = onTabKeystroke
    }
    
    public func makeNSView(context: Context) -> NSTextField {
        let textField = NSTextField()
        textField.stringValue = stringValue
        textField.placeholderString = placeholder
        textField.delegate = context.coordinator
        textField.alignment = .center
        textField.bezelStyle = .roundedBezel
        textField.tag = tag
        return textField
    }
    
    
    public func updateNSView(_ nsView: NSTextField, context: Context) {
        if autoFocus && !didFocus {
            NSApplication.shared.mainWindow?.perform(
                #selector(NSApplication.shared.mainWindow?.makeFirstResponder(_:)),
                with: nsView,
                afterDelay: 0.0
            )
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                didFocus = true
            }
        }
    
        
        if let focusTag = focusTag {
            if focusTag.wrappedValue == nsView.tag {
                NSApplication.shared.mainWindow?.perform(
                    #selector(NSApplication.shared.mainWindow?.makeFirstResponder(_:)),
                    with: nsView,
                    afterDelay: 0.0
                )
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    self.focusTag?.wrappedValue = 0
                }
            }
        }
    }
    
    
    public func makeCoordinator() -> Coordinator {
        Coordinator(with: self)
    }
    
    
    public class Coordinator: NSObject, NSTextFieldDelegate {
        let parent: FocusableTextField
        
        init(with parent: FocusableTextField) {
            self.parent = parent
            super.init()
            
            NotificationCenter.default.addObserver(self,
                                                   selector: #selector(handleAppDidBecomeActive(notification:)),
                                                   name: NSApplication.didBecomeActiveNotification,
                                                   object: nil)
        }
        
        @objc
        func handleAppDidBecomeActive(notification: Notification) {
            if parent.autoFocus && !parent.didFocus {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
                    self.parent.didFocus = false
                }
            }
        }
        
        
        // MARK: - NSTextFieldDelegate Methods
        
        public func controlTextDidChange(_ obj: Notification) {
            guard let textField = obj.object as? NSTextField else { return }
            parent.stringValue = textField.stringValue
            parent.onChange?()
        }
        
        public func control(_ control: NSControl, textShouldEndEditing fieldEditor: NSText) -> Bool {
            parent.stringValue = fieldEditor.string
            parent.onCommit?()
            return true
        }
        
        public func control(_ control: NSControl, textView: NSTextView, doCommandBy commandSelector: Selector) -> Bool {
            if commandSelector == #selector(NSStandardKeyBindingResponding.insertTab(_:)) {
                parent.onTabKeystroke?()
                return true
            }
            return false
        }
    }
}
