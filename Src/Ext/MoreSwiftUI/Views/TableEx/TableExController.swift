
import Foundation
import Cocoa
import SwiftUI
import AppKit

@available(macOS 11.0, *)
public class TableExController<T: RandomAccessCollection, RowView: View>: NSViewController, NSTableViewDelegate, NSTableViewDataSource where T.Index == Int {
    let         id : String
    var         factory: (T.Element, Int) -> RowView
    weak var    tableView: NSTableView?
    let         selection : Binding<Set<Int>>?
    var spec : TableSpec

    
    var items : T
    
    public init(id: String, collection: T, spec : TableSpec, selection : Binding<Set<Int>>?, @ViewBuilder factory: @escaping (T.Element, Int) -> RowView) {
        self.id = id
        self.items = collection
        self.spec = spec
        self.factory = factory
        self.selection = selection
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func viewDidLayout() {
        guard let tableView = tableView else { return }
        tableView.sizeLastColumnToFit()
        
        
//        let spec = items.spec
        
        if spec.shouldResizeScroll,     let scrollView = self.view as? NSScrollView {
            if let vscroll = scrollView.verticalScroller {
                
                var origin = vscroll.frame.origin
                var size   = vscroll.frame.size
                
                origin.y += spec.topMargin
                let newHeight = size.height - (spec.topMargin + spec.bottomMargin)
                
                size.height = max(newHeight, spec.heightOfRow * 2)
                
                vscroll.frame = NSRect(origin: origin, size: size)
            }
        }
    }

    // MARK: - NSTableViewDataSource protocol
    public func numberOfRows(in tableView: NSTableView) -> Int {
        items.count
    }
    
//    public func selectionShouldChange(in tableView: NSTableView) -> Bool {
//        !(selection == nil)
//    }
    
    public func tableView(_ tableView: NSTableView, shouldSelectRow row: Int) -> Bool {
        return true
    }
    

    public func tableViewSelectionDidChange(_ notification: Notification) {
        
        guard let indexes = self.tableView?.selectedRowIndexes else { return }

        DispatchQueue.main.async {
            self.selection?.wrappedValue = Set(indexes)
        }
    }
    
    func set(selection indexes: Set<Int>) {
        tableView?.selectRowIndexes(IndexSet(indexes), byExtendingSelection: false)
    }

    
    public func tableView(_ tableView: NSTableView, heightOfRow row: Int) -> CGFloat {
        spec.heightOfRow
    }
    
    public func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
        return NSHostingView(rootView: factory(items[row], row))
    }
    
    func makeFirstResponder() {
        if tableView?.window == NSApplication.shared.mainWindow {
            NSApplication.shared.mainWindow?.makeFirstResponder(self)
        }
    }
}
