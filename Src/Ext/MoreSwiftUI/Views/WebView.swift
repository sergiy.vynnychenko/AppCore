//
//  WebView.swift
//  TaoGit
//
//  Created by UKS on 20.02.2021.
//  Copyright © 2021 Cheka Zuja. All rights reserved.
//

import SwiftUI
import WebKit

@available(OSX 11.0, *)
public struct WebView: View {
    @Binding var html: String
    @State var dynamicHeight: CGFloat = 10
    @State var staticHeight: CGFloat = -1
    
    public init ( html: Binding<String>, staticHeight: CGFloat = -1 ) {
        _html = html
        self.staticHeight = staticHeight
    }
    
    public var body: some View {
        WebViewWrapper(html: html, dynamicHeight: $dynamicHeight, staticHeight: staticHeight )
            .frame(height: dynamicHeight)
    }
}

@available(OSX 11.0, *)
public struct WebViewWrapper: NSViewRepresentable {
    let html: String
    @Binding var dynamicHeight: CGFloat
    let staticHeight: CGFloat
    // test
    public func makeNSView(context: Context) -> NoScrollWKWebView {
        let a = NoScrollWKWebView()
        a.navigationDelegate = context.coordinator
        
        return a
    }
    
    public func updateNSView(_ webView: NoScrollWKWebView, context: Context) {
        webView.loadHTMLString(html, baseURL: nil)
    }
    
    public func makeCoordinator() -> Coordinator { Coordinator(self, staticHeight: staticHeight) }
}

@available(OSX 11.0, *)
public extension WebViewWrapper {
    class Coordinator: NSObject, WKNavigationDelegate {
        var parent: WebViewWrapper
        let staticHeight: CGFloat
        
        public init(_ parent: WebViewWrapper, staticHeight: CGFloat) {
            self.parent = parent
            self.staticHeight = staticHeight
        }
        
        public func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
            // Disable context menu on webView
            webView.evaluateJavaScript("document.body.setAttribute('oncontextmenu', 'event.preventDefault();');")
            
            // Height fix
            if staticHeight == -1 {
                webView.evaluateJavaScript("document.documentElement.scrollHeight", completionHandler: { (height, error) in
                    DispatchQueue.main.async {
                        if let height = height as? CGFloat {
                            self.parent.dynamicHeight = height
                            AppCore.signals.send(signal: Signal.AppCore.WebViewUpdated(content: self.parent.html, height: self.parent.dynamicHeight) )
                        } else {
                            if self.parent.dynamicHeight == 0 {
                                self.parent.dynamicHeight = 200
                            }
                        }
                    }
                })
            }
            else {
                self.parent.dynamicHeight = staticHeight
            }
        }
    }
}


public class NoScrollWKWebView: WKWebView {
    public override func scrollWheel(with theEvent: NSEvent) {
        nextResponder?.scrollWheel(with: theEvent)
    }
}

public extension Signal {
    struct AppCore {
        public struct WebViewUpdated {
            public var content: String
            public var height: CGFloat
        }
    }
}


@available(macOS 10.15, *)
public struct WebView2: NSViewRepresentable {
    public typealias NSViewType = WKWebView
    
    let webView: WKWebView
    
    public init(url: URL) {
        webView = WKWebView(frame: .zero)
        
        let request = URLRequest(url: url)
        
        webView.load(request as URLRequest)
    }
    
    public func makeNSView(context: Context) -> WKWebView {
        webView
    }
    
    public func updateNSView(_ uiView: WKWebView, context: Context) {
    }
}
