
import Foundation
import Cocoa
import SwiftUI

@available(macOS 10.15, *)
public class CollectionStates : NinjaContext.Main, ObservableObject {
    @Published public var selection : IndexSet
    
    private static let prefix = "AppCore.Collection.Selection."
    
    public init(id: String) {
        let selectionID = CollectionStates.prefix + id
        
        selection = AppCore.states.valueFor(key: selectionID) ?? []
        
        super.init()
        
        AppCore.states.subscribeFor(key: selectionID, valueOfType: IndexSet.self)
            .skipFirst()
            .assign(on: self, to: \.selection)
    }
    
    static func update(selection: IndexSet, id: String) {
        AppCore.states.set(value: selection, forKey: CollectionStates.prefix + id)
    }
}
