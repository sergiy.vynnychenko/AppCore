import SwiftUI
import Quartz
import Combine


@available(macOS 10.15, *)
public struct CollectionView<ItemType: Hashable, Content: View>: NSViewControllerRepresentable  {
    
    //Need to locate here for topScroller
    private var scrollView: NSScrollView = NSScrollView()
    
    private let layout: NSCollectionViewFlowLayout
    
    let items: [ItemType]
    let factory: (ItemType, IndexPath) -> Content
    
    public init(items: [ItemType], layout: NSCollectionViewFlowLayout, factory: @escaping (ItemType, IndexPath) -> Content) {
        self.items = items
        self.layout = layout
        self.factory = factory
        AppCore.log(title: "CollectionView", msg: "init")
    }
    
    public func makeNSViewController(context: Context) -> NSViewController {
        let collectionView = InternalCollectionView()
        scrollView.documentView = collectionView
        
        let viewController = CollectionController(collection: self.items, factory: factory)
        
        viewController.view = scrollView
        scrollView.documentView = collectionView
        
        collectionView.dataSource = viewController
        collectionView.delegate = viewController
        
        collectionView.collectionViewLayout = layout
        collectionView.backgroundColors = [.clear]
        collectionView.isSelectable = true
        collectionView.allowsMultipleSelection = true
        collectionView.allowsEmptySelection = false
//        collectionView.layerContentsRedrawPolicy = .crossfade
        
        collectionView.register(CollectionViewCell.self, forItemWithIdentifier: NSUserInterfaceItemIdentifier("NSCollectionViewItem"))
        
        return viewController
    }
    
    public func updateNSViewController(_ viewController: NSViewController, context: Context) {
        guard let scrollView = viewController.view as? NSScrollView else { return }
        guard let collectionView = scrollView.documentView as? NSCollectionView else { return }
        guard let controller = viewController as? CollectionController<[ItemType],Content> else { return }
        
        
        
        controller.collection = self.items
        collectionView.collectionViewLayout = self.layout
        collectionView.reloadData()

        AppCore.log(title: "CollectionView", msg: "updateNSViewController \(layout.itemSize)")
    }
}


//////////////////////////////
///HELPERS
/////////////////////////////

final class InternalCollectionView: NSCollectionView {
    typealias KeyDownHandler = (_ event: NSEvent) -> Bool
    var keyDownHandler: KeyDownHandler? = nil
    
    override func keyDown(with event: NSEvent) {
        if let keyDownHandler = keyDownHandler {
            let didHandle = keyDownHandler(event)
            if (didHandle) {
                return
            }
        }
        
        super.keyDown(with: event)
    }
    
}

