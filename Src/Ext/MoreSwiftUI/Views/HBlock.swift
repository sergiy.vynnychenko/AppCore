import Foundation
import SwiftUI

@available(macOS 10.15, *)
public struct HBlock: View {
    private let width: CGFloat
    private let views: [AnyView]
        
    public init<Views>(width: CGFloat, @ViewBuilder content: @escaping () -> TupleView<Views>) {
        self.width = width
        views = content().getViews
    }
    
    public var body: some View {
        HStack(spacing: 10) {
            HStack {
                Spacer()
                
                if views.count >= 0 {
                    views[0]
                }
                
            }
            .frame(width: width)
            
            HStack {
                if views.count >= 1 {
                    views[1]
                }
                
                Spacer()
            }
        }
    }
}

@available(macOS 10.15, *)
public extension TupleView {
    var getViews: [AnyView] {
        makeArray(from: value)
    }
    
    private struct GenericView {
        let body: Any
        
        var anyView: AnyView? {
            AnyView(_fromValue: body)
        }
    }
    
    private func makeArray<Tuple>(from tuple: Tuple) -> [AnyView] {
        func convert(child: Mirror.Child) -> AnyView? {
            withUnsafeBytes(of: child.value) { ptr -> AnyView? in
                let binded = ptr.bindMemory(to: GenericView.self)
                return binded.first?.anyView
            }
        }
        
        let tupleMirror = Mirror(reflecting: tuple)
        return tupleMirror.children.compactMap(convert)
    }
}
