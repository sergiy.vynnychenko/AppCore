

import SwiftUI
import AsyncNinja

@available(macOS 10.15, *)
public class FullScreen : NinjaContext.Main, AppLogger, ObservableObject {
    @Published public var isOn : Bool = false
        
    public override init() {
        super.init()
        NotificationCenter.default.publisher(for: NSWindow.willEnterFullScreenNotification)
            .asyncNinja
            .map { _ in true }
            .assign(on: self, to: \.isOn)
        NotificationCenter.default.publisher(for: NSWindow.willExitFullScreenNotification)
            .asyncNinja
            .map { _ in false }
            .assign(on: self, to: \.isOn)
        
        /*
         NotificationCenter.default.addObserver(forName: NSWindow.willEnterFullScreenNotification, object: nil, queue: OperationQueue.main, using: { note in
             print("Entered Fullscreen")
         })
         */
    }
}
