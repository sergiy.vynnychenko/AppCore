import Foundation
import SwiftUI
import MoreSwiftUI

@available(OSX 11.0, *)
public struct AccordionBlock : View {
    @State var collapsed: Bool = false
    @Binding var header: String
    //var subView: some View
    
    public init (header: Binding<String>)//, subView: some View )
    {
        _header = header
        //self.subView = subView
    }
    
    public var body : some View {
        HStack (alignment: .top, spacing: 0) {
            VStack (alignment: .leading, spacing: 0) {
                GeometryReader { geometry in
                    Button ( action: { self.collapsed.toggle() } ){
                        HStack{
                            Text( self.collapsed ? "+" : "-" )
                                .font(.system(size: 20))
                                .animation(.easeInOut)
                                .padding(.leading, 8)
                            
                            Text( self.header )
                                .font(.system(size: 15))
                            
                            Space()
                        }.frame(width: geometry.size.width)
                        
                    }.buttonStyle( PlainButtonStyle() )
                    .background(Color(hex: 0x444444))
                }
                
                if !collapsed {
                    HStack (alignment: .top, spacing: 0) {
                        Text("test")
                        Space()
                    }.padding(5)
                    .background(Color(hex: 0x777777))
                }
                
                Space()
            }
        }
    }
}

