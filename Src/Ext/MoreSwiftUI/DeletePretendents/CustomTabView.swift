//
//  CustomTabView.swift
//  AppCore
//
//  Created by UKS on 17.08.2020.
//  Copyright © 2020 Loki. All rights reserved.
//

import Foundation
import SwiftUI

@available(OSX 11.0, *)
public struct CustomTabView : View {
    @Environment(\.colorScheme) var colorScheme
    
    @State var index: Int
    let tabs: () -> [(String, AnyView)]
    
    public init(forceTab: Int? = nil, tabs: @escaping () -> [(String, AnyView)]){
        _index = State(initialValue: forceTab ?? 0)
        
        self.tabs = tabs
    }
    
    public var body: some View {
        VStack {
            TabsBlock()
            ScrollView(.vertical) {
                PagesBlock()
            }
            .frame(maxWidth: .infinity)
        }
    }
    
    var selectedTabColor: Color { return colorScheme == .dark ?
        Color.white.opacity(0.7) :
        Color.black.opacity(0.7) }
    
    var selectedTabTextColor: Color { return colorScheme == .dark ?
        Color.black :
        Color.white }
    
    var unselectedTabColor: Color { return colorScheme == .dark ?
        Color.white.opacity(0.5) :
        Color.black.opacity(0.5) }
    
    let tabShape = RoundedRectangle(cornerRadius: 8)
}

@available(OSX 11.0, *)
public extension CustomTabView{
    func TabsBlock() -> some View{
        return HStack(spacing: 0){
            ForEach(0..<(tabs().count-1), id: \.self) {
                SingleTab(indx: $0 , title: tabs()[$0].0 )
                //Space()
            }
            
            SingleTab(indx: tabs().count-1, title: tabs().last!.0 )
        }
        .background(unselectedTabColor)
        .clipShape(tabShape)
        .padding(.horizontal)
    }
    
    func PagesBlock() -> some View {
        return tabs()[self.index].1
            .gesture(DragGesture(minimumDistance: 0, coordinateSpace: .local)
                .onEnded { value in
                    if value.translation.width < 0 {
                        if index >= 0 {
                            if( tabs().indices.contains(index+1))
                            {
                                index+=1
                            }
                        }
                    }

                    if value.translation.width > 0 {
                        if( tabs().indices.contains(index-1))
                        {
                            index-=1
                        }
                    }
                 }
            )
    }
    
    fileprivate func SingleTab(indx: Int, title: String) -> some View {
        return Text(title)
            .foregroundColor(selectedTabTextColor)
            .fontWeight(.bold)
            .padding(EdgeInsets(horizontal: 25, vertical: 7))
//            .padding(.vertical,7)
//            .padding(.horizontal,25)
            .background(selectedTabColor.opacity(self.index == indx ? 1 : 0))
            .clipShape(tabShape)
            .background( Color(rgbaHex: 0x00000001) ) // lame hack fix for correct tab change on tap
            .onTapGesture { //withAnimation(.default){
                    
                    self.index = indx
                //}
            }
    }
}

@available(OSX 11.0, *)
public struct TabViewPicker : View {
    @State var selected: Int = 0
    
    public let tabs: [(String, AnyView)]
    
    public init(tabs: [(String, AnyView)]){
        self.tabs = tabs
    }
    
    public var body: some View {
        VStack {
            HStack {
                Picker(selection: $selected, label: Text("")) {
                    ForEach(tabs.indices) { idx in
                        Text(tabs[idx].0)
                            .tag(idx)
                    }
                }
                .pickerStyle(SegmentedPickerStyle())
                .frame(width: 10, alignment: .leading)
                
                Spacer()
            }
            
            tabs[selected].1
        }
    }
}
