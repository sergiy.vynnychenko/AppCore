import Foundation
import SwiftUI

@available(OSX 11.0, *)
public extension View {
    /// Overlays this view with a view that provides a Help Tag.
    func toolTip(_ toolTip: String) -> some View {
        self.overlay(TooltipView(toolTip))
    }
}

@available(OSX 11.0, *)
private struct TooltipView: NSViewRepresentable {
    let toolTip: String
    
    init(_ toolTip: String) {
        self.toolTip = toolTip
    }
    
    func makeNSView(context: NSViewRepresentableContext<TooltipView>) -> NSView {
        NSView()
    }
    
    func updateNSView(_ nsView: NSView, context: NSViewRepresentableContext<TooltipView>) {
        nsView.toolTip = self.toolTip
    }
}
