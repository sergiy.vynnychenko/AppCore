import Foundation
import SwiftUI

@available(OSX 11.0, *)
public extension View {
    func onRClick( action: @escaping () -> () )
        -> some View {
            self.modifier(OnRightClickMod(action: action))
    }
}

@available(OSX 11.0, *)
public struct OnRightClickMod: ViewModifier {
    var action: () -> ()
    
    @ObservedObject var rClickMon: RClickMouseEvents
    
    init(action: @escaping () -> ()) {
        self.action = action
        rClickMon = RClickMouseEvents( action: action )
    }
    
    public func body(content: Content) -> some View {
        content
            .onHover{ rClickMon.isHovering = $0 }
    }
}


////////////////////////////
///HELPERS
////////////////////////////
let rClickMouseMonitor = NSEvent.localMonitor(matching: .rightMouseDown)

@available(OSX 11.0, *)
class RClickMouseEvents : NinjaContext.Main, ObservableObject, AppLogger {
    var isHovering: Bool = false
    
    public init(action: @escaping () -> () ) {
        super.init()
        
        rClickMouseMonitor
            .onUpdate(context: self) { me, event in
                if me.isHovering {
                    action()
                }
            }
    }
}

