import SwiftUI

@available(macOS 10.15, *)
public extension View {
    @ViewBuilder
    func dragHighlighter(_ isDragging: Bool) -> some View{
        if isDragging {
            ZStack {
                self
                
                RoundedRectangle(cornerRadius: 10)
                    .fill(Color(rgbaHex: 0x55555555))
            }
        } else {
            self
        }
    }
}
