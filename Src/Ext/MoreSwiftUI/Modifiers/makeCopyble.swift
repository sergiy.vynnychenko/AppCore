import Foundation
import Combine
import SwiftUI

public struct Copyble {
    public enum Style {
        case alwaysVisible
        case visibleOnHover
        case transparent
    }
    
    public let location: CopyIconLocation
    public let text    : String
    public let message : String
    public let style   : Style
        
    public init(text: String, msg: String = "copied", style: Style = .visibleOnHover, on: CopyIconLocation = .right) {
        self.text = text
        self.message = msg
        self.style = style
        self.location = on
    }
}

public enum CopyIconLocation {
    case left
    case right
    case none
}

@available(OSX 11.0, *)
public extension View {
    func copyble(text: String, msg: String = "copied", style: Copyble.Style = .visibleOnHover, on: CopyIconLocation = .right) -> some View {
        self.modifier(CopybleViewModified(copyble: Copyble(text: text, msg: msg, style: style, on: on)))
    }
    
    func copyble( _ copyble : Copyble) -> some View {
        self.modifier(CopybleViewModified(copyble: copyble))
    }
    
    func makeCopyble(textToCopy: String?, blinkText: String, on location: CopyIconLocation) -> some View {
        self.modifier( MakeCopybleModifier(textToCopy: textToCopy, blinkText: blinkText, isOnLeft: location == .left) )
    }
    
    func makeCopyble(textToCopy: String?, blinkText: String, isOnLeft: Bool? = true) -> some View {
        self.modifier( MakeCopybleModifier(textToCopy: textToCopy, blinkText: blinkText, isOnLeft: isOnLeft) )
    }
}


@available(OSX 11.0, *)
public struct CopybleViewModified: ViewModifier {
    let copyble: Copyble
    @State var isHover = false
    let blinkPublisher = PassthroughSubject<Void, Never>()
    
    public func body(content: Content) -> some View {
        HStack {
            switch copyble.location {
            case .left:
                icon
                content
                
            case .right:
                content
                icon
            case .none:
                content
            }
        }
        .onHover { isHover = $0 }
        .makeFullyIntaractable()
        .opacity(isHover ? 1.0 : 0.8 )
        .onTapGesture { Clipboard.set(text: copyble.text) ; self.blinkPublisher.send() }
        .addTextBlinker(subscribedTo: blinkPublisher, text: copyble.message, duration: 1)
        .animation(.default, value: isHover)
    }
    
    @ViewBuilder
    var icon : some View {
        switch copyble.style {
        case .alwaysVisible:    _icon
        case .transparent:      _icon.opacity(isHover ? 1.0 : 0.2 )
        case .visibleOnHover:   _icon.opacity(isHover ? 0.15 : 0.0 )
        }
    }
    
    var _icon : some View { Text.sfIcon("doc.on.doc.fill", size: 13) }
}


@available(OSX 11.0, *)
public struct MakeCopybleModifier: ViewModifier {
    var textToCopy: String?
    
    var blinkText: String
    
    let blinkPublisher = PassthroughSubject<Void, Never>()
    
    @State var mouseHover = false
    @State var isOnLeft : Bool? = true
    var spacerSize: CGFloat = 5
    
    public func body(content: Content) -> some View {
        trueBody(content: content)
            .makeFullyIntaractable()
            .animation(.default, value: mouseHover)
            .onHover { hovering in mouseHover = hovering }
            .onTapGesture { Clipboard.set(text: textToCopy) ; self.blinkPublisher.send() }
    }
    
    func trueBody(content: Content)-> some View {
        HStack(spacing: 0) {
            if let l = isOnLeft, l {
                CopyMeIcon()
            }
            
            content
                .addTextBlinker(subscribedTo: blinkPublisher, text: blinkText)
            
            if let l = isOnLeft, !l {
                CopyMeIcon()
            }
        }
    }
    
    func CopyMeIcon() -> some View {
        Text.sfIcon("doc.on.doc.fill", size: 13)
            .padding(.init(horizontal: spacerSize, vertical: 0))
            .animationMovementDisable()
            .opacity( mouseHover ? 1 : 0.2)
            .animation(.default, value: mouseHover)
    }
}

////////////////////////////////////////////////////////////////
///Another way to blink
///////////////////////////////////////////////////////////////

@available(OSX 11.0, *)
public extension View {
    func makeCopyble2(textToCopy: String?, on location: CopyIconLocation) -> some View {
        makeCopyble2(textToCopy: textToCopy, isOnLeft: location == .left)
    }
    
    func makeCopyble2(textToCopy: String?, isOnLeft: Bool = true) -> some View {
        self.modifier( MakeCopybleModifier2(textToCopy: textToCopy, isOnLeft: isOnLeft) )
    }
}

@available(OSX 11.0, *)
public struct MakeCopybleModifier2: ViewModifier {
    var textToCopy: String?

    let blinkPublisher = PassthroughSubject<Void, Never>()

    @State var isOnLeft = true
    @State var hover = false
    var spacerSize: CGFloat = 5

    public func body(content: Content) -> some View {
        trueBody(content: content)
            .makeFullyIntaractable()
            .animation(.default, value: hover)
            .onTapGesture { Clipboard.set(text: textToCopy) ; self.blinkPublisher.send() }
    }

    func trueBody(content: Content)-> some View {
        HStack(spacing: 0) {
            if isOnLeft {
                CopyMeIcon()
                    .padding(.trailing, spacerSize)
            }
            
            content
                .opacity( hover ? 0.9 : 1.0 )
                .addOpacityBlinker(subscribedTo: blinkPublisher, duration: 0.3, power: 1)
            
            if !isOnLeft {
                CopyMeIcon()
                    .padding(.leading, spacerSize)
            }
        }.onHover { hover = $0 }
    }
    
    func CopyMeIcon() -> some View {
        Text.sfIcon("doc.on.doc.fill", size: 13)
            .animationMovementDisable()
            .opacity( hover ? 0.7 : 1.0 )
            .padding(.init(horizontal: spacerSize, vertical: 0))
    }
}
