import SwiftUI
import MoreSwiftUI

@available(OSX 12.0, *)
public extension View {
    @ViewBuilder
    func addErrorPanel(error: Binding<Error?>) -> some View {
        if error.wrappedValue != nil {
            let _ = print("ZZZ9 error is not null")
            
            self.overlay {
                    ErrorPanelView(error: error)
                        .animation(.easeInOut)
                }
        } else {
            self
        }
    }
}

@available(macOS 11.0, *)
public struct ErrorPanelView : View {

    @Binding var error: Error?
    var errorStr: String { error?.localizedDescription ?? "Failed to get error string" }
    
    public init(error: Binding<Error?>) {
        _error = error
    }
    
    var needDisplay: Bool { error == nil }
    
    public var body: some View {
        VStack{
            Spacer()
            
            Info()
                .background( BlurredBg() )
                .animation(.default)
        }
        .opacity( needDisplay ? 0 : 1)
        .animation(.easeInOut(duration: 0.4), value: needDisplay)
    }
    
    func BlurredBg() -> some View {
        Space()
            .fillParent()
            .backgroundGaussianBlur(type:.withinWindow, material: .m1_hudWindow)
            .overlay( Color(hex: 0xcc0000).opacity(0.3).theme(.normal(.lvl_4))  )
            .clipShape(RoundedRectangle(cornerRadius: 8))
    }
    
    func Info() -> some View {
        HStack(alignment: .top) {
            Text("Error: \(errorStr)")
                .contextMenu{
                    Button("Copy") {
                        NSPasteboard.general.clearContents()
                        NSPasteboard.general.setString( "\(errorStr)" , forType: NSPasteboard.PasteboardType.string)
                    }
                }
            
            Space()
            Button("x"){
                self.error = nil
            }
        }
        .frame(minHeight:10)
        .padding(13)
    }
}
