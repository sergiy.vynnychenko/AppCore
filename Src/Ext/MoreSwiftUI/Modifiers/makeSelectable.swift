import Foundation
import SwiftUI

@available(OSX 11.0, *)
public extension View {
    @ViewBuilder
    func makeSelectable<T: Equatable>(_ data: Binding<[T]>, selection: T?) -> some View {
        if let selection = selection {
            self.modifier(SelectableMod(data: data, selection: [selection]))
        } else {
            self
        }
    }
    
    func makeSelectable<T: Equatable>(_ data: Binding<[T]>, selection: [T]) -> some View {
        self
            .modifier(SelectableMod(data: data, selection: selection))
    }
    
    func makeSelectable(isSelected: Bool) -> some View  {
        self
            .background( SelectionBg(isSelected: isSelected) )
    }
}

@available(OSX 11.0, *)
public struct SelectableMod<T:Equatable>: ViewModifier {
    @Binding var data: [T]
    let selection: [T]
    
    public func body(content: Content) -> some View {
        if data.contains(where: selection.contains) {
            content
                .background( SelectionBg(isSelected: true) )
        } else {
            content
        }
    }
}

@available(OSX 11.0, *)
fileprivate struct SelectionBg: View {
    let isSelected: Bool
    
    private let selectionColor = Color(hex: 0x0058cf)
    
    var body: some View {
        if isSelected {
            RoundedRectangle(cornerRadius: 8)
                .fill(selectionColor)
        }
    }
}
