//
//  NSImage+Ext.swift
//  AppCore
//
//  Created by loki on 31.05.2020.
//  Copyright © 2020 Loki. All rights reserved.
//

import Foundation
import Essentials

public extension NSImage {
    func hue(radians: Double) -> NSImage {
        guard let cgImage = self.cgImage(forProposedRect: nil, context: nil, hints: nil) else {
            return self
        }
        
        let ciImage = CIImage(cgImage: cgImage)
        guard let filter = CIFilter(name: "CIHueAdjust") else {
            return self
        }
        
        filter.setValue(NSNumber(floatLiteral: radians), forKey: kCIInputAngleKey)
        filter.setValue(ciImage, forKey: kCIInputImageKey)
        guard let outputImage = filter.outputImage else {
            return self
        }
        
        guard let outputCgImage = outputImage.CGImage else {
            return self
        }
        
        return NSImage(cgImage: outputCgImage, size: self.size)
    }
    
    func inverted() -> NSImage {
        guard let cgImage = self.cgImage(forProposedRect: nil, context: nil, hints: nil) else {
            return self
        }
        
        let ciImage = CIImage(cgImage: cgImage)
        guard let filter = CIFilter(name: "CIColorInvert") else {
            return self
        }
        
        filter.setValue(ciImage, forKey: kCIInputImageKey)
        guard let outputImage = filter.outputImage else {
            return self
        }
        
        guard let outputCgImage = outputImage.CGImage else {
            return self
        }
        
        return NSImage(cgImage: outputCgImage, size: self.size)
    }
    
    func scaledCopy( sizeOfLargerSide: CGFloat) ->  NSImage {
        var newW: CGFloat
        var newH: CGFloat
        var scaleFactor: CGFloat
        
        if ( self.size.width > self.size.height) {
            scaleFactor = self.size.width / sizeOfLargerSide
            newW = sizeOfLargerSide
            newH = self.size.height / scaleFactor
        } else{
            scaleFactor = self.size.height / sizeOfLargerSide
            newH = sizeOfLargerSide
            newW = self.size.width / scaleFactor
        }
        
        return resized(w: newW, h: newH)
    }
    
    func resized( w: CGFloat, h: CGFloat) -> NSImage {
        let newSize = NSMakeSize(w, h)
    
        if let bitmapRep = NSBitmapImageRep(
            bitmapDataPlanes: nil, pixelsWide: Int(w), pixelsHigh: Int(h),
            bitsPerSample: 8, samplesPerPixel: 4, hasAlpha: true, isPlanar: false,
            colorSpaceName: .calibratedRGB, bytesPerRow: 0, bitsPerPixel: 0
        ) {
            bitmapRep.size = newSize
            NSGraphicsContext.saveGraphicsState()
            NSGraphicsContext.current = NSGraphicsContext(bitmapImageRep: bitmapRep)
            draw(in: NSRect(x: 0, y: 0, width: newSize.width, height: newSize.height), from: .zero, operation: .copy, fraction: 1.0)
            NSGraphicsContext.restoreGraphicsState()
            
            let resizedImage = NSImage(size: newSize)
            resizedImage.addRepresentation(bitmapRep)
            return resizedImage
        }
        
        return self
    }
    
    func scaledCopy(percent: CGFloat) -> NSImage {
        //size returns incorrect size depend screen, so neede to use sizeReal
        let newW = CGFloat(self.sizeReal.width) / 100.0 * percent
        let newH = CGFloat(self.sizeReal.height) / 100.0 * percent
        
        return resized(w:newW, h: newH)
    }
    
    // Does not work properly
    func resizedCopy(w: CGFloat) -> NSImage {
        let newW = w
        //size returns incorrect size depend screen, so neede to use sizeReal
        let newH = (w/self.sizeReal.width) * self.sizeReal.height
        
        return resized(w:newW, h: newH)
    }
    
    func resizedCopy(h: CGFloat) -> NSImage {
        //size returns incorrect size depend screen, so neede to use sizeReal
        let newW = (h/self.sizeReal.height) * self.sizeReal.width
        let newH = h
        
        return resized(w:newW, h: newH)
    }
    
    private var pngData: Data? {
        guard let tiffRepresentation = tiffRepresentation, let bitmapImage = NSBitmapImageRep(data: tiffRepresentation) else { return nil }
        return bitmapImage.representation(using: .png, properties: [:])
    }
    
    
    func savePng(to url: URL, options: Data.WritingOptions = .atomic) -> R<URL>  {
        return pngData.asNonOptional
            .flatMap{ data in
                Result{ try data.write(to: url, options: options) }
            }
            .map { _ in url}
    }
    
    // quality 1 is max
    func saveJpg(url: URL, quality: Double) -> R<URL> {
        let props: [NSBitmapImageRep.PropertyKey : Any] = [.compressionFactor : NSNumber(floatLiteral: quality), .progressive: 1 ]
        
        return saveImg(url: url, fileType: NSBitmapImageRep.FileType.jpeg, props: props)
    }
    
    func saveJpg2000(url: URL, quality: Double) -> R<URL> {
        //TODO: how to change quality????
        
        return saveImg(url: url, fileType: NSBitmapImageRep.FileType.jpeg, props: [:])
    }
    
//    func saveTiff(url: URL) {
//        let props: [NSBitmapImageRep.PropertyKey : Any] = [.compressionMethod : NSTIFFCompression. ]
//        //https://developer.apple.com/documentation/appkit/nstiffcompression
//
//        return saveImg(url: url, fileType: NSBitmapImageRep.FileType.jpeg, props: props)
//    }
    
    private func saveImg(url: URL, fileType: NSBitmapImageRep.FileType, props: [NSBitmapImageRep.PropertyKey : Any]) -> R<URL> {
        if let bits = self.representations.first as? NSBitmapImageRep {
            let data = bits.representation(using: fileType, properties: props)
            
            return data.asNonOptional
                .map { try? $0.write(to: url) }
                .map { _ in url }
        }
        
        return .wtf("saveImg: failed to get NSBitmapImageRep:  \(fileType)")
    }
}

fileprivate extension CIImage {
    var CGImage: CGImage? {
        if let cgImage = CIContext(options: nil).createCGImage(self, from: self.extent) {
            return cgImage
        }
        return nil
    }
}

extension NSImage {
    var sizeReal: NSSize {
        guard representations.count > 0 else { return NSSize(width: 0, height: 0) }
        
        let rep = self.representations[0]
        return NSSize(width: rep.pixelsWide, height: rep.pixelsHigh)
    }
}
