//
//  NSAttributedString+Ext.swift
//  AppCore
//
//  Created by Loki on 5/16/19.
//  Copyright © 2019 Loki. All rights reserved.
//

import Foundation
import Swift

public extension NSMutableAttributedString {
    private func formatSingle(range: Range<Int>, with attributes: [NSAttributedString.Key : Any]) -> NSMutableAttributedString {
        if range.lowerBound == range.upperBound || range.upperBound > string.count {
            return self
        }

        addAttributes( attributes, range: NSRange(range) )
        return self
    }
    
    func format(ranges: [Range<Int>], with attributes: [NSAttributedString.Key : Any]) -> NSMutableAttributedString {
        var result = self;
        for range in ranges {
            result = result.formatSingle(range: range, with: attributes)
        }
        
        return result
    }
    
    func format(with attributes: [NSAttributedString.Key : Any]) -> NSMutableAttributedString {
        let range = Range<Int>(uncheckedBounds: (0, string.count))
        return formatSingle(range: range, with: attributes)
    }
}

public extension Sequence where Element: NSAttributedString {
    func joinWith(separator: NSAttributedString) -> NSAttributedString {
        var isFirst = true
        return self.reduce(NSMutableAttributedString()) {
            (r, e) in
            if isFirst {
                isFirst = false
            } else {
                r.append(separator)
            }
            r.append(e)
            return r
        }
    }
    
    func joinWith(separator: String) -> NSAttributedString {
        return joinWith(separator: NSAttributedString(string: separator))
    }
}
