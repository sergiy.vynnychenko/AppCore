//
//  NSItemProvider+Ext.swift
//  AppCore
//
//  Created by UKS on 15.08.2020.
//  Copyright © 2020 Loki. All rights reserved.
//

import Foundation
import AsyncNinja

public enum NSItemProviderError : Error {
    case NoURL
}

extension NSItemProviderError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .NoURL:
            return "Error: Can't get url from NSItemProvider"
        }
    }
}

extension NSItemProvider {
    public func getUrl() -> Future<URL> {
        guard self.hasItemConformingToTypeIdentifier("public.file-url") else { return .failed( NSItemProviderError.NoURL ) }
        
        return promise { promise in
            self.loadItem(forTypeIdentifier: "public.file-url", options: nil) {  (coding, error) in
                if let error = error { promise.fail(error) }
                if let coding = coding {
                    guard let data = coding as? Data,
                          let url = URL(dataRepresentation: data, relativeTo: nil)
                    else {
                        promise.fail(NSItemProviderError.NoURL)
                        return
                    }
                    promise.succeed(url)
                }
            }
        }
    }
}
