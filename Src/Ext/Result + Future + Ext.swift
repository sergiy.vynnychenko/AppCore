//
//  Result + Ext.swift
//  AppCore
//
//  Created by UKS on 26.08.2020.
//  Copyright © 2020 Loki. All rights reserved.
//

import Foundation
import AsyncNinja


/////////////////////////////////////////////////////////////////////////////////////////////////////
///Future and channel
/////////////////////////////////////////////////////////////////////////////////////////////////////
public extension Future {
    func asWaitValue() -> Channel<WaitValue<Success, Double>,Void> {
        self.asChannel()
            .map{ WaitValue.value($0) }
            .startWith([WaitValue.wait(-1)]) // No progress
    }
    
    func debugLog(_ msg: String) -> Future<Success> {
        return self.map {
            print(msg)
            return $0
        }
    }
}

public extension Channel {
    func debugLog(_ msg: String) -> Channel<Update,Success> {
        return self.map {
            print(msg)
            return $0
        }
    }
}

public extension ExecutionContext {
    func combineCompleting<ES1:Completing, ES2:Completing>(_ es1: ES1, _ es2: ES2, executor: Executor? = nil)
    -> Future<(ES1.Success, ES2.Success)> {
        
        return Combine2(es1, es2, executor: executor ?? self.executor)
            .retain(with: self)
            .promise
    }
}

fileprivate class Combine2<ES1:Completing, ES2:Completing> : RetainablePromiseOwner<(ES1.Success, ES2.Success)> {
    var success1 : ES1.Success?
    var success2 : ES2.Success?
  
    init(_ es1: ES1, _ es2: ES2, executor: Executor) {
        super.init(executor: executor)
        
        es1
            .onSuccess(context: self) { ctx, success in ctx.success1 = success; ctx.tryComplete()   }
            .onFailure(context: self) { ctx, error in   ctx.promise.fail(error) }
        
        es2
            .onSuccess(context: self) { ctx, success in ctx.success2 = success; ctx.tryComplete()   }
            .onFailure(context: self) { ctx, error in   ctx.promise.fail(error) }
    }
    
    func tryComplete() {
        guard let suc1 = success1 else { return }
        guard let suc2 = success2 else { return }
        
        promise.succeed((suc1,suc2), from: executor)
    }
}

class RetainablePromiseOwner<Success> : ExecutionContext, ReleasePoolOwner {
    public let releasePool = ReleasePool()
    public var executor: Executor
    var promise = Promise<Success>()
    
    init(executor: Executor) {
        self.executor = executor
    }
    
    func retain(with releasePool: Retainer) -> RetainablePromiseOwner {
        releasePool.releaseOnDeinit(self)
        return self
    }
}


/////////////////////////////////////////////////////////////////////////////////////////////////////
///Result
/////////////////////////////////////////////////////////////////////////////////////////////////////
public extension Result {
    func asFuture() -> AsyncNinja.Future<Success> {
        return promise() { promise in
            switch self {
            case let .success(success):
                promise.succeed(success)
            case let .failure(error):
                promise.fail(error)
            }
        }
    }
}


/////////////////////////////////////////////////////////////////////////////////////////////////////
///Other
/////////////////////////////////////////////////////////////////////////////////////////////////////



public enum WaitValue<ValueType, WaitType> {
    case wait(WaitType)
    case value(ValueType)

    public var isWait: Bool {
        if case .wait(_) = self {
            return true
        }
        return false
    }
    
    public var asValue: ValueType? {
        switch self{
            case .value(let value):
                return value
            default:
                return nil
        }
    }
    
    public var asWait: WaitType? {
        switch self{
            case .wait(let value):
                return value
            default:
                return nil
        }
    }
}
