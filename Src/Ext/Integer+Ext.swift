//
//  Integer+Ext.swift
//  AppCore
//
//  Created by Loki on 08.03.2020.
//  Copyright © 2020 Loki. All rights reserved.
//

import Foundation

public extension BinaryInteger {
    var decimalDigitsCount : Int {
        return Int(log10(Double(self))) + 1
    }
}
