/*
 MIT License
 
 Copyright (c) 2014 Sergiy Vynnychenko
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

import Foundation
import Swinject
import os.log
import AsyncNinja
import Essentials

public class AppCore {
    
    public static let env           : ServiceEnvironment = environment()
    public static let scenes        = container.resolve(Scenes.self)!
    public static let signals       : SignalsService = { initSignalSubscribeHandlers(); return SignalsService.main }()
    public static let states        = container.resolve(StatesService.self)!
    public static let bundle        = Bundle.main
    public static var executors     = [String:Executor]()
    
    // Containers
    //public static var mvvmContainer : Container?    // MVVMController resolves ViewModel from this container
    public static let container     = AppCoreContainer(env: env)
    
    public static var logFilters = [String]()
    
    private static var daemons : DaemonsService?
    
    public static var onError : ((String,Error) -> Void)?
    
    /// logs written to ~:  /Users/uks/Library/Containers/com.checkaZuja.focusitoB2/Data/
    public static var writeLogToFile: Bool = false
}

public extension AppCore {
    static func initDaemonService(signals: SignalsService? = nil, container: Container? = nil) {
        if daemons == nil {
            daemons = DaemonsService(container: container ?? AppCore.container)
        }
    }
}

public extension AppCore {
    static func log(title: String, msg: String, thread: Bool = false) {
        guard shouldPass(title: title) else { return }
        
        if thread {
            #if DEBUG
            let thread = Thread.current.dbgName.replace(of: "/Users/loki/dev/", to: "")
                .replace(of: "NSOperationQueue Main Queue", to: "Q MAIN")
            #else
            let thread = Thread.current.dbgName
            #endif
            myPrint("[\(title)]: \(msg) 􀆔\(thread)")
        } else {
            myPrint("[\(title)]: \(msg)")
        }
    }
    
    static func log(title: String, error: Error, thread: Bool = false) {
        onError?(title,error)
        
        guard shouldPass(title: title) else { return }
        if thread {
            myPrint("\n[\(title) ERROR] (\(Thread.current.dbgName)) \(error.localizedDescription)", wrap: true)
        }else {
            myPrint("[\(title) ERROR] \(error.localizedDescription)", wrap: true)
        }
    }
    
    static private func shouldPass(title: String) -> Bool {
        if logFilters.count == 0 {
            return true
        } else {
            return logFilters.contains(title)
        }
    }
    
    static var timestamp : String { return debugDateFormatter.string(from: Date()) }
    
    private static let debugDateFormatter: DateFormatter = { () -> DateFormatter in
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss.SSS"
        return dateFormatter
    }()
}

func myPrint(_ msg: String, wrap: Bool = false) {
    if wrap {
        let txt = "########################################################"
        print(txt)
        logToFileIfNeeded(msg: txt)
    }
    
#if DEBUG
    print("\(AppCore.timestamp) " + msg)
#else
    os_log("%{public}@", msg)
#endif
    
    logToFileIfNeeded(msg: "\(AppCore.timestamp) \(msg)")
    
    if wrap {
        let txt = "########################################################"
        print(txt)
        logToFileIfNeeded(msg: txt)
    }
}

fileprivate var logFileUrl = FSApp.home.appendingPathComponent("LogFile.txt")

fileprivate func logToFileIfNeeded(msg: String) {
    guard AppCore.writeLogToFile else { return }
    
    let file = File(url: logFileUrl )
    
    if !logFileUrl.exists {
        try? file.setContent("")
    }
    
    file.append(msg)
}
