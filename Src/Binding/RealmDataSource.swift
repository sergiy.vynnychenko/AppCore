//
//  RealmDataSource.swift
//  AppCore
//
//  Created by Loki on 4/27/19.
//  Copyright © 2019 Loki. All rights reserved.
//

import Foundation
import AsyncNinja
import RealmSwift

public let PREDICATE_TRUE = NSNumber(value: true)
public let PREDICATE_FALSE = NSNumber(value: false)

public struct RealmChangeset {
    /// the indexes in the collection that were deleted
    public let deleted: [Int]
    
    /// the indexes in the collection that were inserted
    public let inserted: [Int]
    
    /// the indexes in the collection that were modified
    public let updated: [Int]
    
    public init(deleted: [Int], inserted: [Int], updated: [Int]) {
        self.deleted = deleted
        self.inserted = inserted
        self.updated = updated
    }
}

public class RealmDataSource<EntityType: Object> : NinjaContext.Main {
    private var realmQuery          : Results<EntityType>             // initial collection
    private var items               : AnyRealmCollection<EntityType>  // filtered and sorted collection
    private var notificationToken   : NotificationToken?
    
    
    // input
    public var predicate            : NSPredicate?          { didSet { updateSubscription() } }   // filtering
    public var sorting              = [NSSortDescriptor]()  { didSet { updateSubscription() } }
    
    // output
    public var stream : Channel<(AnyRealmCollection<EntityType>, RealmChangeset?), Void> { return producer }
    
    private var producer = Producer<(AnyRealmCollection<EntityType>, RealmChangeset?), Void>()
    
    // input
    public var traceID : String?
    // CollectionSignal.Filter
    // CollectionSignal.Sort
    
    public init(realmQuery: Results<EntityType>, signals: SignalsService?) {
        self.realmQuery = realmQuery
        self.items = realmQuery.toAnyCollection()
        
        super.init()
        
        updateSubscription()
        
        signals?.subscribeFor(Signal.Collection.Filter.self)
            .onUpdate(context: self) { me, signal   in me.predicate = signal.predicate }
        signals?.subscribeFor(Signal.Collection.Sort.self)
            .onUpdate(context: self) { me, signal   in me.sorting = signal.descriptors }
        signals?.subscribeFor(Signal.Collection.ForceReload.self)
            .onUpdate(context: self) { me, _        in me.updateSubscription() }
    }
    
    private func updateSubscription() {
        // apply sorting and filtering
        items = realmQuery.apply(sorting: sorting).apply(predicate: predicate).toAnyCollection()
        
        // update subscription
        notificationToken = items.observe { [weak producer, weak self] changeset in
            if let id = self?.traceID {
                AppCore.log(title: "RealmDataSource", msg: "\(id) changeset \(changeset)")
            }
            
            switch changeset {
            case .initial(let value):
                producer?.update((value, nil))
            case .update(let value, let deletions, let insertions, let modifications):
                producer?.update((value, RealmChangeset(deleted: deletions, inserted: insertions, updated: modifications)))
            case .error(let error):
                producer?.fail(error)
            }
        }
    }
}

public extension ExecutionContext where Self: ReleasePoolOwner {
    func realmChangeset<E>(items: Results<E>) -> Channel<(Results<E>, RealmChangeset?),Void> where E : Object {
        return producer() { me, producer in
            let notificationToken = items.observe { [weak producer] changeset in
                switch changeset {
                case .initial(let value):
                    producer?.update((value, nil))
                case .update(let value, let deletions, let insertions, let modifications):
                    //AppCore.log(title: "realmChangeset", msg: "d: \(deletions), i: \(insertions), m: \(modifications)")
                    producer?.update((value, RealmChangeset(deleted: deletions, inserted: insertions, updated: modifications)))
                case .error(let error):
                    producer?.fail(error)
                }
            }
            me.releasePool.insert(notificationToken)
        }
    }
}

public extension Results where Element : KeypathSortable {
    func apply(sorting: [NSSortDescriptor]) -> Results {
        return sorting.reduce(self) { results, sortDescriptor in results.apply(sorting: sortDescriptor) }
    }
    
    func apply(sorting: NSSortDescriptor) -> Results {
        if let key = sorting.key {
            return sorted(byKeyPath: key, ascending: sorting.ascending)
        }
        return self
    }

    func apply(predicate: NSPredicate?) -> Results {
        if let predicate = predicate {
            return self.filter(predicate)
        }
        return self
    }
    
    func apply(key: String, filter: String) -> Results {
        let predicate = filter.isEmpty ? nil : NSPredicate(format: "\(key) CONTAINS[c] %@", filter)
        return self
            .apply(predicate: predicate)
    }
    
    func apply(key: String, boolVal: Bool) -> Results {
        let predicate = NSPredicate(format: "\(key) == \(boolVal)")
        
        return self
            .apply(predicate: predicate)
    }
    
    func apply(key: String, ascending: Bool) -> Results {
        let sort = NSSortDescriptor(key: key, ascending: ascending)
        return self
            .apply(sorting:   sort)
    }
}
