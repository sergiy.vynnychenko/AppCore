/*
 Apple script possible to use only if you have Accessibility access
*/

import Foundation
import Essentials

public class AppleScript {
    static func executeSync(_ script: String) -> R<(String)> {
        DispatchQueue(label: "AppleScript", qos: .background).sync {
            var error: NSDictionary?
            var outputStr: String?
            
            if let scriptObject = NSAppleScript(source: script) {
                let output = scriptObject.executeAndReturnError(&error)
                
                outputStr = output.stringValue
            }
            
            if let outputStr = outputStr {
                return .success(outputStr)
            }
            
            if let error = error {
                let errorStr = error.allKeys.map{ "\($0):\(String(describing: error[$0]!)); " }.joined()
                
                return .failure(WTF(errorStr))
            }
            
            return .failure(WTF("Why this error happens? It's impossible"))
        }
    }
}

public extension AppleScript {
    static func getInfo(of urls: [URL]) -> R<String> {
        let files = urls.map{ "POSIX file \"\($0.path)\", " }.joined().trimEnd([","," "])
        
        let str = """
                  set fileList to {\(files)}
                  
                  tell application "Finder"
                      activate
                      set selection to fileList
                      tell application "System Events"
                          keystroke "i" using {command down, option down}
                      end tell
                  end tell
                  """
        
        return AppleScript.executeSync(str)
    }
}
