//
//  KeychainTests.swift
//  AppCoreTests
//
//  Created by loki on 13.06.2021.
//  Copyright © 2021 Loki. All rights reserved.
//

import XCTest
@testable import AppCore
import Essentials
import EssentialsTesting

@available(macOS 10.15, *)
class KeychainTests: XCTestCase {
    
//    func testDelete() {
//        let test_url = "git@gitlab.com:sergiy.vynnychenko/test_public.git"
//
//        Keychain.delete(url: test_url)
//            .shouldFail("delete")
//    }
    
    func test_keyShouldAddAndFind() {
        let set = KeychainSet(name: "github", service: "com.taogit.test")
        
        print(set.key)
        
        set.items                   // we expect keychain record do not exist at this stage
            .assertEqual(to: [])
        
        let tokenEntry = KeychainSet.TokenEntry(token: "2113C04D615ECB08", alias: "user@email.com")
        let tokenEntry2 = KeychainSet.TokenEntry(token: "2c8ff89f05ECB03", alias: "user2@email.com")
        
        set.append(token: tokenEntry)
            .shouldSucceed("append")
        
        set.items
            .assertEqual(to: [tokenEntry], "items")
        
        set.append(token: tokenEntry2)
            .shouldSucceed("append")
        
        set.items                   // we expect keychain record do not exist at this stage
            .assertEqual(to: [tokenEntry, tokenEntry2], "items")
        
        set.delete(token: tokenEntry)
            .shouldSucceed("delete 1")
        
        set.delete(token: tokenEntry2)
            .shouldSucceed("delete 1")
        
        set.items                   // we expect keychain record stores an empty array
            .assertEqual(to: [], "items empty")
        
        set.clean()
            .shouldSucceed("clean")
        
//        set.
        
//        keyDic.lookup(key: key)
//            .shouldFail("should fail lookup")
//        
//        keyDic.addSafe(key: key, value: value)
//            .shouldSucceed("addSafe")
//        
//        keyDic.addSafe(key: key, value: value)
//            .shouldFail("addSafe same value")
//        
//        keyDic.lookup(key: key).assertEqual(to: value)
//        keyDic.lookup(key: key).assertNotEqual(to: newValue)
//        
//        keyDic.update(key: key, value: newValue)
//            .shouldSucceed("update")
//        
//        keyDic.lookup(key: key).assertEqual(to: newValue)
//        keyDic.lookup(key: key).assertNotEqual(to: value)
        
//        keyDic.all()
//            .shouldSucceed("all")
//        
//        keyDic.delete(key: key)
//            .shouldSucceed("delete")
    }
    
    func test_genericPassword() {
        let account = "/Users/loki/.ssh/test_id_rsa"
        let pass = "querty"
        let service = "TaoGit.SSH.PrivateKey"
        
        Keychain.lookup(account: account, serivce: service)
            .shouldFail()
        
        Keychain.addSafe(account: .generic(account: account, pass: pass, service: service))
            .shouldSucceed()
        
        Keychain.lookup(url: account)
            .shouldFail()
        Keychain.lookup(account: account, serivce: service)
            .shouldSucceed()
        
        // should faild next time
        Keychain.addSafe(account: .generic(account: account, pass: pass, service: service))
            .shouldFail()
        
        // should succeed
        Keychain.addForced(account: .generic(account: account, pass: pass, service: service))
            .shouldSucceed()
        
        Keychain.delete(service: service)
            .shouldSucceed()
    }
    
    func test_SshPrivateKey() {
        let private_key_path = "/Users/loki/.ssh/id_rsa"
        let private_key_pass = "querty"
        
        Keychain.lookup(url: private_key_path)
            .shouldFail()
        
        Keychain.addSafe(account: .sshPrivate(key: private_key_path, pass: private_key_pass))
            .shouldSucceed()

        // should faild next time
        Keychain.addSafe(account: .sshPrivate(key: private_key_path, pass: private_key_pass))
            .shouldFail()
        
        // should succeed
        Keychain.addForced(account: .sshPrivate(key: private_key_path, pass: private_key_pass))
            .shouldSucceed()

        Keychain.lookup(url: private_key_path)
            .shouldSucceed()

//        Keychain.all()
//            .assertBlock { $0.count == 1 }
        
        Keychain.delete(url: private_key_path)
            .shouldSucceed()
        
//        Keychain.all()
//            .assertBlock { $0.count == 0 }
        

//
//        Keychain.delete(url: test_url)
//            .assertFailure("delete")
//

    }
}
