//import Essentials
//import Foundation
//@testable import AppCore
//import XCTest
//
//extension Result {
//    @discardableResult
//    func assertBlock(_ topic: String? = nil, block: (Success) -> Bool) -> Success? {
//        onSuccess {
//            topic?.print(success: $0)
//            XCTAssert(block($0))
//        }.onFailure {
//            topic?.print(failure: $0)
//            XCTAssert(false)
//        }
//        return maybeSuccess
//    }
//
//
//    @discardableResult
//    func assertFailure(_ topic: String? = nil) -> Success? {
//        onSuccess {
//            topic?.print(success: $0)
//        }.onFailure {
//            topic?.print(failure: $0)
//            XCTAssert(false)
//        }
//        return maybeSuccess
//    }
//
//    @discardableResult
//    func assertSuccess(_ topic: String? = nil) -> Success? {
//        onSuccess {
//            topic?.print(success: $0)
//            XCTAssert(false)
//        }.onFailure {
//            topic?.print(failure: $0)
//        }
//        return maybeSuccess
//    }
//
//    @discardableResult
//    func assertEqual(to: Success, _ topic: String? = nil) -> Success? where Success: Equatable {
//        onSuccess {
//            XCTAssert(to == $0)
//            topic?.print(success: $0)
//        }.onFailure {
//            topic?.print(failure: $0)
//            XCTAssert(false)
//        }
//        return maybeSuccess
//    }
//
//    @discardableResult
//    func assertNotEqual(to: Success, _ topic: String? = nil) -> Success? where Success: Equatable {
//        onSuccess {
//            XCTAssert(to != $0)
//            topic?.print(success: $0)
//        }.onFailure {
//            topic?.print(failure: $0)
//            XCTAssert(false)
//        }
//        return maybeSuccess
//    }
//
//    var maybeSuccess: Success? {
//        switch self {
//        case let .success(s):
//            return s
//        default:
//            return nil
//        }
//    }
//}
//
//extension String {
//    func print<T>(success: T) {
//        Swift.print("\(self) SUCCEEDED with: \(success)")
//    }
//
//    func print(failure: Error) {
//        Swift.print("\(self) FAILED with: \(failure.localizedDescription)")
//    }
//
//    func write(to file: URL) -> Result<Void, Error> {
//        do {
//            try write(toFile: file.path, atomically: true, encoding: .utf8)
//            return .success(())
//        } catch {
//            return .failure(error)
//        }
//    }
//}


