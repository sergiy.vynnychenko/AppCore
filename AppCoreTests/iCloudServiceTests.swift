
import XCTest
import CloudKit
import AsyncNinja
@testable import AppCore

//class iCloudServiceTests: XCTestCase {
//
//    let recordType = "TestRecord"
//    let cloudPrivate = AppCore.container.resolve(iCloundNinjaPrivate.self)!
//
//    override func setUp() {
//        XCTAssert(cloudPrivate.container.status().wait().maybeSuccess == .available)
//        // Put setup code here. This method is called before the invocation of each test method in the class.
//    }
//
//    override func tearDown() {
//        // Put teardown code here. This method is called after the invocation of each test method in the class.
//    }
//
//    func testDelete() {
//        //cloudPrivate.batchSize = 100
//        let (_, completion) = cloudPrivate.deleteRecordsOf(type: recordType).waitForAll()
//        XCTAssert(completion.maybeSuccess != nil)
//    }
//
//    func testFetch() {
//        cloudPrivate.batchSize = 100
//        let (result, _) = cloudPrivate.fetchRecordsOf(type: recordType).waitForAll()
//        print(result.map { $0.count } )
//    }
//
//    func testPushFetchDelete() {
//        let COUNT = 5
//
//        let cloudDB = cloudPrivate.cloudDB
//        let pushResult = cloudDB.push(records: fakeRecords(count: COUNT)).wait().maybeSuccess!
//        XCTAssert(pushResult.count == COUNT)
//        sleep(3)
//
//        let IDs = pushResult.map { $0.recordID }
//
//        let fetchResult = cloudDB.fetch(IDs: IDs).wait().maybeSuccess!
//        XCTAssert(fetchResult.count == COUNT)
//
//        let deleteResult = cloudDB.delete(IDs: IDs).wait()
//        XCTAssert(deleteResult.maybeSuccess?.count == COUNT)
//    }
//
//    func testQuery() {
//        let (_,delComp0) = cloudPrivate.deleteRecordsOf(type: recordType).waitForAll()
//        XCTAssert(delComp0.maybeSuccess != nil)
//
//        let PUSH_COUNT = 350
//        cloudPrivate.batchSize = 100
//        let (pushResult, pushComp) = cloudPrivate.push(records: fakeRecords(count: PUSH_COUNT)).waitForAll()
//        XCTAssert(pushComp.maybeSuccess != nil)
//        XCTAssert(pushResult.flatMap { $0 }.count == PUSH_COUNT)
//
//        sleep(3)
//
//        let(queryResult, queryComp) = cloudPrivate.fetchRecordsOf(type: recordType).waitForAll()
//        XCTAssert(queryComp.maybeSuccess != nil)
//        XCTAssert(queryResult.flatMap { $0 }.count == PUSH_COUNT)
//
//        let (delResult, delComp) = cloudPrivate.deleteRecordsOf(type: recordType).waitForAll()
//        XCTAssert(delComp.maybeSuccess != nil)
//        XCTAssert(delResult.flatMap { $0 }.count == PUSH_COUNT)
//        print("delResult \(delResult.flatMap { $0 }.count)")
//    }
//
//    func testPushAndDelete2() {
//        let COUNT = 8
//
//
//        let cloudDB = cloudPrivate.cloudDB
//        cloudPrivate.batchSize = 2
//        let (result, completion) = cloudPrivate.push(records: fakeRecords(count: COUNT)).waitForAll()
//
////        let records = result.flatMap { $0 }
////
////        XCTAssert(completion.maybeSuccess != nil)
////        XCTAssert(result.count == COUNT / cloudPrivate.batchSize)
////        XCTAssert(records.count == COUNT)
////
////        let deleteResult = cloudDB.delete(IDs: records.map { $0.recordID }).wait()
////        XCTAssert(deleteResult.success?.count == COUNT)
//    }
//
//
//    func testPushOld() {
//        let cloudDB = cloudPrivate.cloudDB
//
//        let pushResult = cloudDB.push(records: fakeRecords(count: 5)).wait()
//
//
//
//
//        /// PUSH with many batches
//        cloudPrivate.batchSize = 2
//        let count = 10
//        let (result, completion) = cloudPrivate.push(records: fakeRecords(count: count)).waitForAll()
//        let records = result.flatMap { $0 }
//
//        print("result \(result.count)")
//
//        XCTAssert(completion.maybeSuccess != nil)
//        XCTAssert(result.count == count / cloudPrivate.batchSize)
//        XCTAssert(records.count == count)
//
//        /// PUSH with single batch
//        cloudPrivate.batchSize = 20
//        let (result2, completion2) = cloudPrivate.push(records: fakeRecords(count: count)).waitForAll()
//        let records2 = result.flatMap { $0 }
//
//        XCTAssert(completion2.maybeSuccess != nil)
//        XCTAssert(result2.count == 1)
//        XCTAssert(records2.count == count)
//
//        // wait for server processing
//        //sleep(3)
//
//        //let allRecords = records + records2
//        //let allIDs = allRecords.map { $0.recordID }
//
//        //        let (resultFetch, completeFetch) = cloudPrivate.fetchRecordsOf(type: recordType).waitForAll()
//        //        let recordsFetch = resultFetch.flatMap { $0 }
//        //
//        //        XCTAssert(recordsFetch.count == allRecords.count)
//
//        //let fetchAll = cloudPrivate.fetchRecordsOf(type: recordType).map { $0.map { $0.recordID } }
//        //_ = cloudPrivate.delete(IDs: fetchAll).waitForAll()
//    }
//
//    func testPerformanceExample() {
//        // This is an example of a performance test case.
//        self.measure {
//            // Put the code you want to measure the time of here.
//        }
//    }
//
//    func testUserRecordID() {
//        let userRecordID = cloudPrivate.userRecordID().wait()
//        XCTAssert(userRecordID.maybeSuccess != nil)
//
//        //cloudPrivate.userRecordID()
//    }
//
//}
//
//extension iCloudServiceTests {
//    func fakeRecords(count: Int) -> [CKRecord] {
//        var records = [CKRecord]()
//        for _ in 0 ..< count {
//            records.append(CKRecord(recordType: recordType))
//        }
//        return records
//    }
//}
//
//func flatMapTest(arg: String) -> Channel<String,Void> {
//    return producer(executor: .serialUnique) { producer in
//        for i in 0...100 {
//            producer.update("_\(arg)_\(i)")
//        }
//    }
//}




