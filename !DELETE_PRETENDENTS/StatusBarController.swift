//
//  StatusBarController.swift
//  SwiftCore
//
//  Created by Loki on 1/5/19.
//  Copyright © 2019 checkazuja. All rights reserved.
//

import Foundation
import AsyncNinja

public extension Signal {
    struct StatusBar {
        public struct Click { public init() {} }
        public struct Visible {
            public let isVisible : Bool
            public init(_ value: Bool) {
                self.isVisible = value
            }
        }
        public struct SetImage {
            public let img: NSImage
            public init(_ img: NSImage) {
                self.img = img
            }
        }
    }
}

public class StatusBarController : NSObject, AppLogger {
    private let icon         = NSStatusBar.system.statusItem(withLength: NSStatusItem.variableLength)
    private var popover      :  NSPopover?
    
    public override init() {
        super.init()
        
// fixed warning code
//        icon.button?.actionChannel().onUpdate { _ in
//            self.onClick()
//        }
        
        icon.button?.action = #selector(onClick)
        icon.button?.target = self
    }
    
    public func set(visible: Bool) {
        log(msg: "visible: \(visible)", thread: true)
        DispatchQueue.main.async {
            self.icon.isVisible = visible
        }
    }
    
    public func set(img: NSImage, inverted: Bool) {
        log(msg: "going to set image", thread: true)
        DispatchQueue.main.async {
            self.icon.button?.image = inverted ? img.inverted() : img
        }
    }
    
    public func set(menu: NSMenu) {
        log(msg: "going to set menu", thread: true)
        DispatchQueue.main.async {
            self.icon.menu = menu
        }
    }
    
    public func setPopOver(controller: NSViewController) {
        log(msg: "going to set popOver", thread: true)
        DispatchQueue.main.async {
            if self.popover == nil {
                self.popover = NSPopover()
                self.popover!.behavior = NSPopover.Behavior.transient
            }
            self.popover?.contentViewController = controller
        }
    }
}

private extension StatusBarController {
    @objc func onClick() {
        switchPopOver()
        AppCore.signals.send(signal: Signal.StatusBar.Click())
    }

    func switchPopOver() {
        guard let pop = popover else { return }
        
        showPopOver(!pop.isShown)
    }
    
    func showPopOver(_ show: Bool) {
        guard
            let btn = icon.button,
            let pop = popover else { return }
        
        if show {
            pop.show(relativeTo: btn.bounds, of: btn, preferredEdge: NSRectEdge.minY)
            startMonitor()
        } else {
            pop.performClose(self)
        }
    }
    
    func startMonitor() {
        NSEvent.globalMonitor(matching: [.leftMouseDown, .rightMouseDown])
            .takeOne()
            .onUpdate(context: popover!) { [weak self] _, _ in self?.showPopOver(false) }
    }
}

